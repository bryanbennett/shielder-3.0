﻿using Shielder_3._0.Data;
using Shielder_3._0.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PaleHorseStudios.Shielder;

namespace Shielder_3._0
{
    public partial class Main : Form
    {
        private Point dragAnchor;  //the point at which the drag was initiated
        private bool isDragging;
        private Dictionary<ViewState, BaseView> views;
        private ShielderSettings ss;
        private bool showSplash = false; //debug
        private string settingsPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\settings.bin";

        public Main()
        {
            InitializeComponent();

            if (showSplash)
            {
                this.Hide();
                using (Splash splash = new Splash())
                {
                    splash.ShowDialog();
                }
            }

            dragAnchor = new Point(0, 0);
            Init();
            LoadViewStates();
            ChangeView(ViewState.Start);
        }
        /// <summary>
        /// Initializes Shielder.
        /// </summary>
        private void Init()
        {
            if (File.Exists(settingsPath))
            {
                Console.WriteLine(settingsPath);
                Stream streamer = File.OpenRead(settingsPath);
                BinaryFormatter ds = new BinaryFormatter();
                ss = (ShielderSettings)ds.Deserialize(streamer);
                streamer.Close();
            }
            else
            {
                ss = new ShielderSettings();
            }
        }
        #region "View Handling"
        /// <summary>
        /// Loads all views into the dictionary.
        /// </summary>
        private void LoadViewStates()
        {
            views = new Dictionary<ViewState, BaseView>();
            WorkView work = new WorkView(this);
            views.Add(ViewState.Work, work);
            StartView start = new StartView(this);
            views.Add(ViewState.Start, start);
        }
        /// <summary>
        /// Changes the view based on the value of the ViewState argument.
        /// </summary>
        /// <param name="state"></param>
        public void ChangeView(ViewState state)
        {
            pnlMain.Controls.Clear();
            pnlMain.Controls.Add(views[state]);
        }
        #endregion
        #region "Properties"
        /// <summary>
        /// Returns the initialized settings for Shielder.
        /// </summary>
        public ShielderSettings Settings { get { return ss; } }
        #endregion
        #region "Title Bar and Drag"
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btnMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        private void pnlTitleBar_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && !isDragging)
            {
                isDragging = true;
                dragAnchor = new Point(e.X, e.Y);
            }
        }
        private void pnlTitleBar_MouseMove(object sender, MouseEventArgs e)
        {
            if (isDragging)
            {
                Bounds = new Rectangle(Bounds.X + e.X - dragAnchor.X, Bounds.Y + e.Y - dragAnchor.Y, Bounds.Width, Bounds.Height);
            }
        }
        private void pnlTitleBar_MouseUp(object sender, MouseEventArgs e)
        {
            isDragging = false;
        }
#endregion
        #region "Ready Bar"
        /// <summary>
        /// Updates the group and item counts found in the lower right hand corner of the window.
        /// Also sets the labels to be visible.
        /// </summary>
        /// <param name="p"></param>
        public void UpdateCounts(Package p)
        {
            lblGroupCount.Visible = true;
            lblItemCount.Visible = true;
            lblGroupCount.Text = "Groups: " + GetCount(Layer.Group, p, 0);
            lblItemCount.Text = "Items: " + GetCount(Layer.Item, p, 0);
        }
        /// <summary>
        /// Returns the amount of nodes with the specified layer.
        /// </summary>
        /// <param name="layer"></param>
        /// <returns></returns>
        private int GetCount(Layer layer, ShielderNode node, int count)
        {
            if (node.Layer == layer)
            {
                count += 1;
            }
            if (node.Nodes.Count > 0)
            {
                int subCount = 0;
                foreach (ShielderNode sNode in node.Nodes)
                {
                    count += GetCount(layer, sNode, subCount);
                }
            }
            return count;
        }
        /// <summary>
        /// Sets the visibility of the save icon.
        /// </summary>
        /// <param name="val"></param>
        public void ShowSave(bool val)
        {
            pbxSave.Visible = val;
        }

        /// <summary>
        /// Sets the status text in the lower left hand corner.
        /// </summary>
        /// <param name="status"></param>
        public void SetStatus(string status)
        {
            lblStatus.Text = status;
        }
        #endregion
        public void NewPackage(Package package)
        {
            WorkView work = (WorkView) views[ViewState.Work];
            work.GenerateTree(package);
            ChangeView(ViewState.Work);
        }
        /// <summary>
        /// Returns the specified View.
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        public BaseView GetView(ViewState state)
        {
            return views[state];
        }
        private void Main_Load(object sender, EventArgs e)
        {

        }

        private void pnlTitleBar_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pnlMain_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            Stream streamer = File.Create(settingsPath);
            BinaryFormatter f = new BinaryFormatter();
            f.Serialize(streamer, ss);
            streamer.Close();
        }

        private void rectangleShape1_Click(object sender, EventArgs e)
        {

        }





    }
}
