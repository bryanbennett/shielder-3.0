﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Shielder_3._0.Views;
using PaleHorseStudios.Shielder;

namespace Shielder_3._0.Details
{
    public partial class StringDetail : Detail
    {
        public StringDetail(Definition def, WorkView work)
            :base(def, work)
        {
            InitializeComponent();
            txtValue.Text = def.Default;
            txtValue.MaxLength = def.CharLimit;
        }
        public StringDetail()
        {
            InitializeComponent();
        }

        private void StringDetail_Load(object sender, EventArgs e)
        {

        }

        private void txtValue_TextChanged(object sender, EventArgs e)
        {
            ItemData.Value = txtValue.Text;
        }
        public override void SetItemData(ItemData data)
        {
            txtValue.Text = data.Value;
        }
        #region "Properties"

        #endregion
    }
}
