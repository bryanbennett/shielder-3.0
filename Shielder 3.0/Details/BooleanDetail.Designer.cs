﻿namespace Shielder_3._0.Details
{
    partial class BooleanDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkValue = new System.Windows.Forms.CheckBox();
            this.pnlMain = new System.Windows.Forms.FlowLayoutPanel();
            this.SuspendLayout();
            // 
            // chkValue
            // 
            this.chkValue.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkValue.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.chkValue.Location = new System.Drawing.Point(3, 1);
            this.chkValue.Name = "chkValue";
            this.chkValue.Size = new System.Drawing.Size(152, 24);
            this.chkValue.TabIndex = 2;
            this.chkValue.UseVisualStyleBackColor = true;
            this.chkValue.CheckedChanged += new System.EventHandler(this.chkValue_CheckedChanged);
            // 
            // pnlMain
            // 
            this.pnlMain.Location = new System.Drawing.Point(-1, 31);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(250, 34);
            this.pnlMain.TabIndex = 3;
            // 
            // BooleanDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.chkValue);
            this.Controls.Add(this.pnlMain);
            this.Name = "BooleanDetail";
            this.Size = new System.Drawing.Size(250, 78);
            this.Load += new System.EventHandler(this.BooleanDetail_Load);
            this.Controls.SetChildIndex(this.btnDelete, 0);
            this.Controls.SetChildIndex(this.btnDown, 0);
            this.Controls.SetChildIndex(this.btnUp, 0);
            this.Controls.SetChildIndex(this.lblLabel, 0);
            this.Controls.SetChildIndex(this.pnlMain, 0);
            this.Controls.SetChildIndex(this.chkValue, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkValue;
        private System.Windows.Forms.FlowLayoutPanel pnlMain;
    }
}
