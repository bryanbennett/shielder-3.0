﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Shielder_3._0.Views;
using PaleHorseStudios.Shielder;

namespace Shielder_3._0.Details
{
    /// <summary>
    /// This detail handles any numerical input.
    /// </summary>
    public partial class NumericDetail : Detail
    {
        public NumericDetail(Definition def, WorkView work)
            : base(def, work)
        {
            InitializeComponent();
            numValue.Value = Decimal.Parse(def.Default);
            numValue.DecimalPlaces = def.DecimalPlaces;
            numValue.Maximum = def.MaxValue;
            numValue.Minimum = def.MinValue;
            numValue.Increment = (decimal)(1/(Math.Pow(10, def.DecimalPlaces)));
            Child = def.IsChild;
        }
        public NumericDetail()
        {
            InitializeComponent();
        }

        private void NumericDetail_Load(object sender, EventArgs e)
        {

        }

        private void numValue_ValueChanged(object sender, EventArgs e)
        {
            if (!AllowModification)
            {
                ItemData.Value = numValue.Value.ToString();
            }
            else
            {
                Definition.Default = numValue.Value.ToString();
            }
        }
        public override void SetItemData(ItemData data)
        {
            numValue.Value = Decimal.Parse(data.Value);
        }
        #region "Properties"

        #endregion
    }
}
