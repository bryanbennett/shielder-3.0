﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Shielder_3._0.Views;
using PaleHorseStudios.Shielder;

namespace Shielder_3._0.Details
{
    /// <summary>
    /// This Detail handles boolean input.  This class is also the only class that can have child details.
    /// </summary>
    public partial class BooleanDetail : Detail
    {
        private bool value;
        private Dictionary<string, Detail> childDetails;
        private bool groupSet = false;

        public BooleanDetail(Definition def, WorkView work)
            : base(def, work)
        {
            InitializeComponent();
            childDetails = new Dictionary<string, Detail>();
            lblLabel.Visible = false;
            chkValue.Text = def.Name;
            bool defaultVal = false;
            bool succeeded = Boolean.TryParse(def.Default, out defaultVal);
            if (succeeded)
            {
                chkValue.Checked = defaultVal;
            }
            else
            {
                chkValue.Checked = false;
            }
        }
        public BooleanDetail()
        {
            InitializeComponent();
        }

        private void BooleanDetail_Load(object sender, EventArgs e)
        {

        }
        private void chkValue_CheckedChanged(object sender, EventArgs e)
        {
            value = chkValue.Checked;
            if (value)
            {
                ItemData.Value = "True";
            }
            else
            {
                ItemData.Value = "False";
            }

            //enables or disables the child details.
            int count = childDetails.Count;
            if (count > 0 && !groupSet)
            {
                foreach (KeyValuePair<string, Detail> entry in childDetails)
                {
                    Detail child = entry.Value;
                    if (value)
                    {
                        child.Enabled = true;
                    }
                    else
                    {
                        child.Enabled = false;
                    }
                }
            }
        }
        /// <summary>
        /// Adds a child to this boolean detail.  The children are enabled or disabled based on the Checked value
        /// of the checkbox control.  If groupSet is set to true, then the modification buttons are enabled and visible.
        /// </summary>
        /// <param name="child"></param>
        public void AddChild(Detail child, bool groupSet)
        {
            this.groupSet = groupSet;
            childDetails.Add(child.DetailName, child);
            pnlMain.Height = pnlMain.Height + child.Height;
            this.Height = child.Height + this.Height;
            pnlMain.Controls.Add(child);
            child.SetParent(this);

            if (groupSet)
            {
                child.AllowModification = true;
            }
            else
            {
                child.AllowModification = false;
                if (!chkValue.Checked)
                {
                    child.Enabled = false;
                }
            }
        }
        public void ReOrder(int upOrDown, Detail detail)
        {
            ControlCollection controls = pnlMain.Controls;
            int index = controls.IndexOf(detail);

            if (index + upOrDown < controls.Count)
            {
                controls.SetChildIndex(detail, index + upOrDown);
            }
            else
            {
                controls.SetChildIndex(detail, 0);
            }
        }
        public override void SetItemData(ItemData data)
        {
            bool result;
            bool success = Boolean.TryParse(data.Value, out result);
            chkValue.Checked = result;
        }
        /// <summary>
        /// Retrieves the ItemData belonging to child details of this control.
        /// </summary>
        /// <returns></returns>
        public List<ItemData> ChildData()
        {
            List<ItemData> datum = new List<ItemData>();
            if (HasChildDefinitions)
            {
                int count = childDetails.Count;

                foreach (KeyValuePair<string, Detail> entry in childDetails)
                {
                    datum.Add(entry.Value.ItemData);
                }
            }
            return datum;
        }
        #region "Properties"
        /// <summary>
        /// Returns the boolean value of this boolean detail.
        /// </summary>
        public bool Value { get { return value; } }

        /// <summary>
        /// Returns whether or not this Detail has child definitions.
        /// </summary>
        public bool HasChildDefinitions
        {
            get
            {
                if (childDetails.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        #endregion
    }
}
