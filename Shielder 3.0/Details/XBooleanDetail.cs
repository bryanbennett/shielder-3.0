﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Shielder_3._0.Views;
using PaleHorseStudios.Shielder;

namespace Shielder_3._0.Details
{
    /// <summary>
    /// This Detail handles user-designed information that populates the combobox.
    /// </summary>
    public partial class XBooleanDetail : Detail
    {

        public XBooleanDetail(Definition def, WorkView work)
            : base(def, work)
        {
            InitializeComponent();
            cbxValue.SelectedIndex = 0;
            int count = def.List.Length;
            for (int i = 0; i < count; i++)
            {
                cbxValue.Items.Add(def.List[i]);
            }
        }
        public XBooleanDetail()
        {
            InitializeComponent();
        }

        private void XBooleanDetail_Load(object sender, EventArgs e)
        {

        }

        private void cbxValue_SelectedIndexChanged(object sender, EventArgs e)
        {
            ItemData.Value = cbxValue.SelectedItem.ToString();
        }
        #region "Properties"

        #endregion

        public override void SetItemData(ItemData data)
        {
            string value = data.Value;

            int count = cbxValue.Items.Count;

            for (int i = 0; i < count; i++)
            {
                string cbxVal = (string)cbxValue.Items[i];
                if (cbxVal.Equals(value))
                {
                    cbxValue.SelectedIndex = i;
                    break;
                }
            }
        }
    }
}
