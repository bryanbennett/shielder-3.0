﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Shielder_3._0.Views;
using PaleHorseStudios.Shielder;
using System.IO;

namespace Shielder_3._0.Details
{
    /// <summary>
    /// This Detail handles references to images such as spritesheets or sprites.
    /// </summary>
    public partial class ImageDetail : Detail
    {
        private SpritePack pack;
        private Dictionary<string,Image> images;
        public ImageDetail(Definition def, WorkView work)
            : base(def, work)
        {
            InitializeComponent();
            pack = work.Package.SpritePacks[def.SpritePack];
            images = pack.ToDictionary();
            BuildComboBox();
        }
        public ImageDetail()
        {
            InitializeComponent();
        }

        private void ImageDetail_Load(object sender, EventArgs e)
        {

        }
        private void BuildComboBox()
        {
            foreach (KeyValuePair<string, Image> entry in images)
            {
                cbxValue.Items.Add(entry.Key);
            }
        }

        public override void SetItemData(ItemData data)
        {
            string value = data.Value;

            int count = cbxValue.Items.Count;

            for (int i = 0; i < count; i++)
            {
                string cbxVal = (string)cbxValue.Items[i];
                if (cbxVal.Equals(value))
                {
                    cbxValue.SelectedIndex = i;
                    break;
                }
            }
        }

        private void cbxValue_SelectedIndexChanged(object sender, EventArgs e)
        {
            ItemData.Value = cbxValue.SelectedItem.ToString();
            if (cbxValue.SelectedIndex == 0)
            {
                pbxPreview.Visible = false;
            }
            else
            {
                pbxPreview.Visible = true;
                pbxPreview.Image = images[cbxValue.SelectedItem.ToString()];
            }
        }
    }
}
