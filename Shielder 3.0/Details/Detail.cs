﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Shielder_3._0.Views;
using PaleHorseStudios.Shielder;
using Shielder_3._0.Views.Editors;

namespace Shielder_3._0.Details
{
    /// <summary>
    /// The base class that all Details subclass.
    /// </summary>
    public partial class Detail : UserControl
    {
        private bool isChild;
        private string parentName;
        private WorkView work;
        private bool allowModification;
        private Definition def;
        private BooleanDetail parentDetail;
        private ItemData data;

        public Detail(Definition def, WorkView work)
        {
            InitializeComponent();
            this.def = def;
            this.work = work;
            lblLabel.Text = def.Name;
            AllowModification = false;
            this.Name = def.Name;
            this.Visible = true;
            data = new ItemData(def.Name);
            data.Value = def.Default;
        }
        public Detail()
        {
            InitializeComponent();
        }

        private void Detail_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Sets the parent of this detail, sets Child = true, and sets the Borderstyle of the detail to none.
        /// </summary>
        /// <param name="parent"></param>
        public void SetParent(BooleanDetail parent)
        {
            this.parentDetail = parent;
            isChild = true;
            parentName = parent.DetailName;
            def.ParentDefinition = parentName;
            this.BorderStyle = BorderStyle.None;
        }
        public virtual void SetItemData(ItemData data)
        {

        }
        #region "Detail Modification"
        private void btnDown_Click(object sender, EventArgs e)
        {
            if (!isChild)
            {
                work.ReOrder(1, this);
            }
            else
            {
                parentDetail.ReOrder(1, this);
            }
        }

        private void btnUp_Click(object sender, EventArgs e)
        {
            if (!isChild)
            {
                work.ReOrder(-1, this);
            }
            else
            {
                parentDetail.ReOrder(-1, this);
            }
        }
        private void btnEdit_Click(object sender, EventArgs e)
        {
            XDetailEditor x = new XDetailEditor(def, (Group)work.SelectedNode, work);
            x.ShowDialog();
            x.Dispose();
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            work.DeleteDefinition(this);
        }
        #endregion
        #region "Properties"
        /// <summary>
        /// Returns the type of data this Detail represents.
        /// </summary>
        public Datatype Datatype { get { return def.Type; } }

        /// <summary>
        /// Returns the name of this detail.
        /// </summary>
        public string DetailName { get { return def.Name; } }

        /// <summary>
        /// Returns or sets whether or not this detail is a child detail of a parent detail.
        /// </summary>
        public bool Child { get { return isChild; } set { isChild = value; } }

        /// <summary>
        /// Returns the name of the Parent of this detail.  If this detail
        /// does not have a parent, it returns "".
        /// </summary>
        public string DetailParent { get { if (isChild) { return parentName; } else { return ""; } } }

        /// <summary>
        /// Gets or sets whether or not this detail allows the use of the sorting buttons.
        /// </summary>
        public bool AllowModification
        {
            get
            {
                return allowModification;
            }
            set
            {
                allowModification = value;
                if (!allowModification)
                {
                    btnDown.Enabled = false;
                    btnDown.Visible = false;
                    btnUp.Enabled = false;
                    btnUp.Visible = false;
                    btnEdit.Enabled = false;
                    btnEdit.Visible = false;
                    btnDelete.Enabled = false;
                    btnDelete.Visible = false;
                }
                else
                {
                    btnDown.Enabled = true;
                    btnDown.Visible = true;
                    btnUp.Enabled = true;
                    btnUp.Visible = true;
                    btnEdit.Enabled = true;
                    btnEdit.Visible = true;
                    btnDelete.Enabled = true;
                    btnDelete.Visible = true;
                }
            }
        }

        /// <summary>
        /// Returns the Definition associated with this detail.
        /// </summary>
        public Definition Definition { get { return def; } }

        /// <summary>
        /// Gets or sets the ItemData associated with this detail.
        /// </summary>
        public ItemData ItemData { get { return data; } set { data = value; } }

        /// <summary>
        /// Returns the work view currently modifying details in this execution.
        /// </summary>
        public WorkView WorkView { get { return work; } }
        #endregion
    }
}
