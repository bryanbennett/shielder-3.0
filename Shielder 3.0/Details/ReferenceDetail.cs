﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Shielder_3._0.Views;
using PaleHorseStudios.Shielder;

namespace Shielder_3._0.Details
{
    /// <summary>
    /// This class handles the detail information that references the items in other groups.
    /// </summary>
    public partial class ReferenceDetail : Detail
    {
        private Group group;

        public ReferenceDetail(Definition def, WorkView work)
            :base(def, work)
        {
            InitializeComponent();
            this.group = work.GetGroup(def.Reference);
            Child = def.IsChild;
            GenerateStringValues();
        }
        public ReferenceDetail()
        {
            InitializeComponent();
        }
        private void GenerateStringValues()
        {
            List<Item> items = group.GetItems();
            foreach (Item item in items)
            {
                cbxValue.Items.Add(item.Text);
            }
        }
        private void ReferenceDetail_Load(object sender, EventArgs e)
        {

        }

        public override void SetItemData(ItemData data)
        {
            string value = data.Value;

            int count = cbxValue.Items.Count;

            for (int i = 0; i < count; i++)
            {
                string cbxVal = (string) cbxValue.Items[i];
                if (cbxVal.Equals(value))
                {
                    cbxValue.SelectedIndex = i;
                    break;
                }
            }
        }

        private void cbxValue_SelectedIndexChanged(object sender, EventArgs e)
        {
            string val = cbxValue.SelectedItem.ToString();
            ItemData.Value = val;
        }
    }
}
