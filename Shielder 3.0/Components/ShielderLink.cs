﻿using Shielder_3._0.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shielder_3._0.Components
{
    public partial class ShielderLink : LinkLabel
    {
        private StartView start;
        private string id;

        public ShielderLink(StartView start, string id)
        {
            InitializeComponent();
            this.start = start;
            this.id = id;
        }
        public ShielderLink()
        {
            InitializeComponent();
        }

        public ShielderLink(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        private void ShielderLink_MouseClick(object sender, MouseEventArgs e)
        {
            start.RecentLabelClick(id);
        }
        /// <summary>
        /// Returns the ID associated with this ShielderLink.
        /// </summary>
        public string ID { get { return id; } }
    }
}
