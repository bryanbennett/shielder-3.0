﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shielder_3._0.Components
{
    public partial class DarkDropDown : System.Windows.Forms.ToolStripDropDown
    {
        public DarkDropDown()
        {
            InitializeComponent();
            Init();
        }

        public DarkDropDown(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
            Init();
        }
        private void Init()
        {
            Color back = Color.FromArgb(0, 40, 40, 40);
            this.BackColor = back;

        }
    }
}
