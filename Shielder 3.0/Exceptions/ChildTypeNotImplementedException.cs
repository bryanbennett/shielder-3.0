﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shielder_3._0.Exceptions
{
    public class ChildTypeNotImplementedException : Exception
    {
        public ChildTypeNotImplementedException() { }
        public ChildTypeNotImplementedException(string message)
            :base(message)
        {

        }
    }
}
