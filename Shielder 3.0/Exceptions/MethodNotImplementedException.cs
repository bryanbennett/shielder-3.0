﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shielder_3._0.Exceptions
{
    public class MethodNotImplementedException : Exception
    {

        public MethodNotImplementedException()
        {

        }
        public MethodNotImplementedException(string message)
            : base(message)
        {

        }

    }
}
