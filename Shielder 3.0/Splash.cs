﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shielder_3._0
{
    public partial class Splash : Form
    {
        private int count = 0;
        private int maxCount = 3;
        public Splash()
        {
            InitializeComponent();
            timer.Start();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            count += 1;
            if (count >= maxCount)
            {
                this.Dispose();
            }
        }

        private void Splash_FormClosing(object sender, FormClosingEventArgs e)
        {
            timer.Stop();
        }
    }
}
