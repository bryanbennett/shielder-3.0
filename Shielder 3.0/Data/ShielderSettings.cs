﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shielder_3._0.Data
{
    [Serializable()]
    public class ShielderSettings
    {
        private Dictionary<string, string> packages;
        private Stack<string> recentPackages;

        public ShielderSettings()
        {
            packages = new Dictionary<string, string>();
            recentPackages = new Stack<string>();
        }
        /// <summary>
        /// Adds a package to the recent packages list and the list of packages created by this Shielder.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="path"></param>
        public void AddPackage(string guid, string path)
        {
            packages.Add(guid, path);
            recentPackages.Push(guid);
            ReorderRecent(guid);
        }
        /// <summary>
        /// Removes the specified package from the collections.
        /// </summary>
        /// <param name="id"></param>
        public void RemovePackage(string guid)
        {
            packages.Remove(guid);
            Stack<string> inverseStack = new Stack<string>();

            //Searches through the stack for the desired ID, storing all popped ID's not matching
            //the removal ID in an inverse stack.

            int count = recentPackages.Count;
            for (int i = 0; i < count; i++)
            {
                string packID = recentPackages.Pop();
                if (!packID.Equals(guid))
                {
                    inverseStack.Push(packID);
                }
            }

            //Reconstructs the recent stack and maintains the correct order.
            count = inverseStack.Count;
            for (int i = 0; i < count; i++)
            {
                recentPackages.Push(inverseStack.Pop());
            }
            
        }
        /// <summary>
        /// Retrieves the path of the specified package.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string GetPackagePath(string guid)
        {
            return packages[guid];
        }
        /// <summary>
        /// Reorders the recent list by placing the desired package ID at the top of the stack.
        /// </summary>
        /// <param name="id"></param>
        public void ReorderRecent(string guid)
        {
            Stack<string> inverse = new Stack<string>();
            int count = recentPackages.Count;

            //takes out the ID in the stack by not pushing it to the twin stack.
            for (int i = 0; i < count; i++)
            {
                string entry = recentPackages.Pop();
                if (!entry.Equals(guid))
                {
                    inverse.Push(entry);
                }
            }

            count = inverse.Count;

            //rebuilds the recentPackages stack with the same order (but without the updated element)
            for (int i = 0; i < count; i++)
            {
                recentPackages.Push(inverse.Pop());
            }

            //re-adds the element to the top of the stack.
            recentPackages.Push(guid);
        }
        public bool PackageExists(string guid)
        {
            return packages.ContainsKey(guid);
        }

        #region "Properties"

        /// <summary>
        /// Returns the stack containing the ID's of recently edited or opened packages.
        /// </summary>
        public Stack<string> Recent { get { return recentPackages; } }

        /// <summary>
        /// Returns the amount of packages stored in this ShielderSettings.
        /// </summary>
        public int StoredPackagesCount { get { return packages.Count; } }
        #endregion


    }
}
