﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shielder_3._0.Views
{
    public partial class ShielderMessageBox : Form
    {
        public ShielderMessageBox(string title, string prompt)
        {
            InitializeComponent();
            lblTitle.Text = title;
            rtbPrompt.Text = prompt;
            this.ActiveControl = lblTitle;
        }
        public ShielderMessageBox()
        {
            InitializeComponent();
        }

        private void ShielderMessageBox_Load(object sender, EventArgs e)
        {

        }
        private void rtbPrompt_Enter(object sender, EventArgs e)
        {
            //This makes it so the rich text box cannot be selected.
            ActiveControl = lblTitle;
        }
    }
}
