﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shielder_3._0.Views
{
    public partial class OKMessage : ShielderMessageBox
    {
        public OKMessage(string title, string message)
            :base(title, message)
        {
            InitializeComponent();
        }
        public OKMessage()
        {
            InitializeComponent();
        }

        private void OKMessage_Load(object sender, EventArgs e)
        {

        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
