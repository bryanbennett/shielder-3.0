﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shielder_3._0.Views
{
    public partial class YESNOMessage : ShielderMessageBox
    {
        public YESNOMessage(string title, string prompt)
            : base(title, prompt)
        {
            InitializeComponent();
        }
        public YESNOMessage()
        {
            InitializeComponent();
        }

        private void YESNOMessage_Load(object sender, EventArgs e)
        {

        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        private void btnNo_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
