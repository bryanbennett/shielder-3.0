﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shielder_3._0.Views
{
    /// <summary>
    /// Base class for "Views" which are basically forms.  BaseView sets the background to gray, which all Views will have.  The class also
    /// provides a reference to the Main class.
    /// </summary>
    public partial class BaseView : UserControl
    {
        private Main main;

        public BaseView(Main main)
        {
            InitializeComponent();
            this.main = main;
        }
        public BaseView()
        {
            InitializeComponent();
        }

        #region "Properties"

        public Main Main { get { return main; } }

        #endregion

        private void BaseView_Load(object sender, EventArgs e)
        {

        }
    }
}
