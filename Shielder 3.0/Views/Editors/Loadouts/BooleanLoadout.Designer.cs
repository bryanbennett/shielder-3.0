﻿namespace Shielder_3._0.Views.Editors.Loadouts
{
    partial class BooleanLoadout
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAddDetail = new System.Windows.Forms.Button();
            this.cbxDataTypes = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // btnAddDetail
            // 
            this.btnAddDetail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.btnAddDetail.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddDetail.Location = new System.Drawing.Point(156, 3);
            this.btnAddDetail.Name = "btnAddDetail";
            this.btnAddDetail.Size = new System.Drawing.Size(123, 23);
            this.btnAddDetail.TabIndex = 0;
            this.btnAddDetail.Text = "Add Child Detail";
            this.btnAddDetail.UseVisualStyleBackColor = false;
            this.btnAddDetail.Click += new System.EventHandler(this.btnAddDetail_Click);
            // 
            // cbxDataTypes
            // 
            this.cbxDataTypes.FormattingEnabled = true;
            this.cbxDataTypes.Location = new System.Drawing.Point(3, 3);
            this.cbxDataTypes.Name = "cbxDataTypes";
            this.cbxDataTypes.Size = new System.Drawing.Size(147, 21);
            this.cbxDataTypes.TabIndex = 1;
            this.cbxDataTypes.SelectedIndexChanged += new System.EventHandler(this.cbxDataTypes_SelectedIndexChanged);
            // 
            // BooleanLoadout
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cbxDataTypes);
            this.Controls.Add(this.btnAddDetail);
            this.Name = "BooleanLoadout";
            this.Size = new System.Drawing.Size(369, 30);
            this.Load += new System.EventHandler(this.BooleanLoadout_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnAddDetail;
        private System.Windows.Forms.ComboBox cbxDataTypes;
    }
}
