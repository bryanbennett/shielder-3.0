﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Shielder_3._0.Exceptions;
using PaleHorseStudios.Shielder;

namespace Shielder_3._0.Views.Editors.Loadouts
{
    public partial class Loadout : UserControl
    {
        private Datatype type;
        private SettingsData data;
        public Loadout()
        {
            InitializeComponent();
        }
        public Loadout(Datatype type, SettingsData data)
        {
            InitializeComponent();
            this.type = type;
            this.Visible = true;
            this.data = data;
            Point location = new Point(0, 0);
            this.Location = location;
        }

        private void Loadout_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Must-override method that returns the values contained in the datatype's loadout.  If the base method is
        /// called, a MethodNotImplementedException will be thrown.
        /// </summary>
        /// <returns></returns>
        public virtual string[] GetValues()
        {
            string message = "The GetValues() method was not implemented for detail type " + type.ToString() + ".";
            throw new MethodNotImplementedException(message);
        }
        /// <summary>
        /// Must-override method that makes changes to items in case a rule was changed or its name was changed.
        /// If overriding, ensure that the base method IS called, unlike the other methods that must
        /// be overriden and implemented.  The base method returns null.
        /// </summary>
        /// <param name="oldName"></param>
        /// <param name="items"></param>
        /// <param name="def"></param>
        public virtual List<string> ValidateItems(string oldName, List<Item> items, Definition def)
        {
            NameChange(oldName, items, def.Name);
            return null;
        }
        /// <summary>
        /// Must-Override method that returns whether or not the loadout's values are acceptable.
        /// </summary>
        /// <returns></returns>
        public virtual bool VerifyInput()
        {
            string message = "The VerifyInput() method was not implemented for detail type " + type.ToString() + ".";
            throw new MethodNotImplementedException(message);
        }
        private void NameChange(string oldName, List<Item> items, string newName)
        {
            if (!oldName.Equals(newName))
            {
                foreach (Item item in items)
                {
                    if (item.ItemDataExists(oldName))
                    {
                        ItemData data = item.GetData(oldName);
                        item.RemoveData(oldName);
                        data.DefinitionName = newName;
                        item.SaveData(data);
                    }
                }
            }
        }
        #region "Properties"
        /// <summary>
        /// Returns the loadout datatype.
        /// </summary>
        public Datatype Type { get { return type; } }

        /// <summary>
        /// Returns the settings data contained in the package node.
        /// </summary>
        public SettingsData SettingsData { get { return data; } }
        #endregion
    }
}
