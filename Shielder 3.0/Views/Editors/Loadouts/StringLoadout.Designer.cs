﻿namespace Shielder_3._0.Views.Editors.Loadouts
{
    partial class StringLoadout
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.numCharLimit = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.numCharLimit)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "Character Limit:";
            // 
            // numCharLimit
            // 
            this.numCharLimit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.numCharLimit.ForeColor = System.Drawing.SystemColors.Control;
            this.numCharLimit.Location = new System.Drawing.Point(113, 3);
            this.numCharLimit.Name = "numCharLimit";
            this.numCharLimit.Size = new System.Drawing.Size(140, 21);
            this.numCharLimit.TabIndex = 17;
            this.numCharLimit.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // StringLoadout
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.numCharLimit);
            this.Name = "StringLoadout";
            this.Size = new System.Drawing.Size(261, 29);
            this.Load += new System.EventHandler(this.StringLoadout_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numCharLimit)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numCharLimit;


    }
}
