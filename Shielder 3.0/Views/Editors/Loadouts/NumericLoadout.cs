﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PaleHorseStudios.Shielder;

namespace Shielder_3._0.Views.Editors.Loadouts
{
    public partial class NumericLoadout : Loadout
    {
        public NumericLoadout(WorkView work)
            :base(PaleHorseStudios.Shielder.Datatype.Numeric, work.Package.Settings)
        {
            InitializeComponent();
        }
        public NumericLoadout(Definition def,WorkView work)
            :base(Datatype.Numeric, work.Package.Settings)
        {
            InitializeComponent();
            numDecimal.Value = def.DecimalPlaces;
            numMax.Value = def.MaxValue;
            numMin.Value = def.MinValue;
        }
        private void NumericLoadout_Load(object sender, EventArgs e)
        {

        }
        public override string[] GetValues()
        {
            string[] values = new string[3];
            values[0] = numMin.Value.ToString();
            values[1] = numMax.Value.ToString();
            values[2] = numDecimal.Value.ToString();
            return values;
        }
        public override List<string> ValidateItems(string oldName, List<Item> items, Definition def)
        {
            base.ValidateItems(oldName, items, def);
            List<string> changedItems = new List<string>();

            foreach (Item item in items)
            {
                if (item.ItemDataExists(def.Name))
                {
                    ItemData data = item.GetData(def.Name);
                    decimal value = Decimal.Parse(data.Value);
                    decimal max = def.MaxValue;
                    decimal min = def.MinValue;
                    bool hasChanged = false;
                    if (value > max)
                    {
                        value = max;
                        hasChanged = true;
                    }
                    if (value < min)
                    {
                        value = min;
                        hasChanged = true;
                    }
                    if (hasChanged)
                    {
                        data.Value = value.ToString();
                        changedItems.Add(item.Parent.Text + "\\" + item.Text);
                        item.ForeColor = SettingsData.ChangedColor;
                        item.SaveData(data);
                    }
                }
            }
            return changedItems;
        }
        public override bool VerifyInput()
        {
            return true;
        }

        private void numDecimal_ValueChanged(object sender, EventArgs e)
        {
            decimal val = numDecimal.Value;
            decimal inc = (decimal)(1 / (Math.Pow(10, (double)val)));
            numMin.DecimalPlaces = (int)val;
            numMin.Increment = inc;

            numMax.DecimalPlaces = (int)val;
            numMax.Increment = inc;
        }


    }
}
