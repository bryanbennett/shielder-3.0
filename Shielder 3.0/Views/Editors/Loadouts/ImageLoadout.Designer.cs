﻿namespace Shielder_3._0.Views.Editors.Loadouts
{
    partial class ImageLoadout
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbxSpritePack = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cbxSpritePack
            // 
            this.cbxSpritePack.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxSpritePack.FormattingEnabled = true;
            this.cbxSpritePack.Location = new System.Drawing.Point(86, 6);
            this.cbxSpritePack.Name = "cbxSpritePack";
            this.cbxSpritePack.Size = new System.Drawing.Size(194, 21);
            this.cbxSpritePack.TabIndex = 22;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "Sprite Pack:";
            // 
            // ImageLoadout
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cbxSpritePack);
            this.Controls.Add(this.label4);
            this.Name = "ImageLoadout";
            this.Size = new System.Drawing.Size(286, 33);
            this.Load += new System.EventHandler(this.ImageLoadout_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbxSpritePack;
        private System.Windows.Forms.Label label4;
    }
}
