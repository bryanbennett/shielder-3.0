﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PaleHorseStudios.Shielder;

namespace Shielder_3._0.Views.Editors.Loadouts
{
    public partial class ImageLoadout : Loadout
    {
        private string oldSpritePack = "";

        public ImageLoadout(WorkView work)
            :base(Datatype.Image, work.Package.Settings)
        {
            InitializeComponent();
            BuildComboBox(work);
        }
        public ImageLoadout(Definition def, WorkView work)
            :base(Datatype.Image, work.Package.Settings)
        {
            InitializeComponent();
            BuildComboBox(work);
            oldSpritePack = def.SpritePack;
            int count = cbxSpritePack.Items.Count;
            for (int i = 0; i < count; i++)
            {
                cbxSpritePack.SelectedIndex = i;
                string item = cbxSpritePack.SelectedItem.ToString();
                if (item.Equals(def.SpritePack))
                {
                    break;
                }
                if (i == count - 1)
                {
                    cbxSpritePack.SelectedIndex = -1;
                }
            }

        }
        public override bool VerifyInput()
        {
            if (cbxSpritePack.SelectedIndex < 0)
            {
                OKMessage ok = new OKMessage("Select Sprite Pack", "You must select a SpritePack before creating an ImageDetail.");
                ok.ShowDialog();
                return false;
            }
            return true;
        }
        public override List<string> ValidateItems(string oldName, List<Item> items, Definition def)
        {
            base.ValidateItems(oldName, items, def);
            List<string> changedItems = new List<string>();

            foreach (Item item in items)
            {
                if (item.ItemDataExists(def.Name))
                {
                    ItemData data = item.GetData(def.Name);

                    if (!oldSpritePack.Equals(def.SpritePack))
                    {
                        changedItems.Add(item.Parent.Text + "\\" + item.Text);
                        data.Value = "";
                        item.ForeColor = SettingsData.ChangedColor;
                    }
                    item.SaveData(data);
                }
            }
            return changedItems;
        }
        public override string[] GetValues()
        {
            string[] values = new string[1];
            values[0] = cbxSpritePack.SelectedItem.ToString();
            return values;
        }
        private void BuildComboBox(WorkView work)
        {
            Dictionary<string, SpritePack> packs = work.Package.SpritePacks;

            foreach (KeyValuePair<string, SpritePack> entry in packs)
            {
                cbxSpritePack.Items.Add(entry.Key);
            }
        }

        private void ImageLoadout_Load(object sender, EventArgs e)
        {

        }
    }
}
