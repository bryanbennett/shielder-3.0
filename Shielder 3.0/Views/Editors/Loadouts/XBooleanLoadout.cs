﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PaleHorseStudios.Shielder;

namespace Shielder_3._0.Views.Editors.Loadouts
{
    public partial class XBooleanLoadout : Loadout
    {
        public XBooleanLoadout(WorkView work)
            :base(Datatype.XBoolean, work.Package.Settings)
        {
            InitializeComponent();
        }
        public XBooleanLoadout(Definition def, WorkView work)
            : base(Datatype.XBoolean, work.Package.Settings)
        {
            InitializeComponent();
            string text = "";
            int count = def.List.Length;
            for (int i = 0; i < count; i++)
            {
                text += def.List[i] + "\n";
            }
            rtbList.Text = text;
        }
        public override string[] GetValues()
        {
            string entry = rtbList.Text;
            string[] list = entry.Split(new string[] { Environment.NewLine, "\r\n", "\n" }, StringSplitOptions.None);
            return list;
        }
        public override List<string> ValidateItems(string oldName, List<Item> items, Definition def)
        {
            base.ValidateItems(oldName, items, def);
            List<string> changedItems = new List<string>();

            foreach (Item item in items)
            {
                if (item.ItemDataExists(def.Name))
                {
                    ItemData data = item.GetData(def.Name);
                    string[] list = def.List;
                    int length = list.Length;
                    bool hasChanged = true;
                    for (int i = 0; i < length; i++)
                    {
                        string listItem = list[i];
                        if (listItem.Equals(data.Value))
                        {
                            hasChanged = false;
                            break;
                        }
                    }
                    if (hasChanged)
                    {
                        data.Value = "";
                        changedItems.Add(item.Parent.Text + "\\" + item.Text);
                        item.ForeColor = SettingsData.ChangedColor;
                    }
                }
            }
            return changedItems;
        }
        public override bool VerifyInput()
        {
            if (rtbList.Text.Equals(""))
            {
                OKMessage ok = new OKMessage("Input Text", "You must enter in each line item of the detail into the text box.");
                ok.ShowDialog();
                ok.Dispose();
                return false;
            }
            return true;
        }
        private void XBooleanLoadout_Load(object sender, EventArgs e)
        {

        }
    }
}
