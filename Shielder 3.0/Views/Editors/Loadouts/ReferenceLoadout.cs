﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PaleHorseStudios.Shielder;

namespace Shielder_3._0.Views.Editors.Loadouts
{
    public partial class ReferenceLoadout : Loadout
    {
        private WorkView work;

        public ReferenceLoadout(WorkView work)
            :base(Datatype.Reference, work.Package.Settings)
        {
            InitializeComponent();
            BuildComboBox(work);
            this.work = work;
        }
        public ReferenceLoadout(Definition def, WorkView work)
            : base(Datatype.Reference, work.Package.Settings)
        {
            InitializeComponent();
            BuildComboBox(work);
            this.work = work;

            int count = cbxGroup.Items.Count;

            for (int i = 0; i < count; i++)
            {
                cbxGroup.SelectedIndex = i;
                string text = cbxGroup.SelectedItem.ToString();
                if (text.Equals(def.Reference))
                {
                    break;
                }
                if (i == count - 1)
                {
                    cbxGroup.SelectedIndex = -1;
                }
            }
        }
        private void BuildComboBox(WorkView work)
        {
            List<Group> groups = work.GetGroups();

            foreach (Group group in groups)
            {
                cbxGroup.Items.Add(group.Text);
            }
        }
        public override List<string> ValidateItems(string oldName, List<Item> items, Definition def)
        {
            base.ValidateItems(oldName, items, def);
            List<string> changedItems = new List<string>();

            foreach (Item item in items)
            {
                if (item.ItemDataExists(def.Name))
                {
                    ItemData data = item.GetData(def.Name);
                    string value = data.Value;
                    Group group = work.GetGroup(cbxGroup.SelectedItem.ToString());
                    List<Item> refItems = work.GetItemsOfGroup(group);
                    bool hasChanged = true;

                    foreach (Item referenced in refItems)
                    {
                        string text = referenced.Text;
                        if (text.Equals(value))
                        {
                            hasChanged = false;
                            break;
                        }
                    }
                    if (hasChanged)
                    {
                        data.Value = "";
                        changedItems.Add(item.Parent.Text + "\\" + item.Text);
                        item.ForeColor = SettingsData.ChangedColor;
                        item.SaveData(data);
                    }
                }
            }


            return changedItems;
        }
        public override bool VerifyInput()
        {
            if (cbxGroup.SelectedIndex < 0)
            {
                OKMessage ok = new OKMessage("Select Group", "You must select a group to reference.");
                ok.ShowDialog();
                ok.Dispose();
                return false;
            }
            return true;
        }
        public override string[] GetValues()
        {
            string[] values = new string[1];
            values[0] = cbxGroup.SelectedItem.ToString();
            return values;
        }


        private void ReferenceLoadout_Load(object sender, EventArgs e)
        {

        }
    }
}
