﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PaleHorseStudios.Shielder;
using Shielder_3._0.Exceptions;

namespace Shielder_3._0.Views.Editors.Loadouts
{
    public partial class BooleanLoadout : Loadout
    {
        private Datatype selected;
        private Group group;
        private WorkView work;
        private List<Definition> childDefs;
        public BooleanLoadout(Group group, WorkView work, List<Definition> childDefs)
            :base(Datatype.Boolean, work.Package.Settings)
        {
            InitializeComponent();
            this.group = group;
            this.work = work;
            this.childDefs = childDefs;
            BuildComboBox();
        }
        public BooleanLoadout(Group group, WorkView work, Definition def, List<Definition> childDefs)
            : base(Datatype.Boolean, work.Package.Settings)
        {
            InitializeComponent();
            this.group = group;
            this.work = work;
            this.childDefs = childDefs;
            BuildComboBox();
        }
        private void BuildComboBox()
        {
            Datatype[] types = (Datatype[])Enum.GetValues(typeof(Datatype));
            int length = types.Length;
            for (int i = 0; i < length; i++)
            {
                cbxDataTypes.Items.Add(types[i].ToString());
            }
        }
        public override bool VerifyInput()
        {
            return true;
        }
        public override List<string> ValidateItems(string oldName, List<Item> items, Definition def)
        {
            base.ValidateItems(oldName, items, def);
            List<string> changedItems = new List<string>();
            //TODO
            return changedItems;
        }
        public override string[] GetValues()
        {
            string[] values = new string[0];
            return values;
        }
        private void BooleanLoadout_Load(object sender, EventArgs e)
        {

        }

        private void btnAddDetail_Click(object sender, EventArgs e)
        {
            if (cbxDataTypes.SelectedIndex >= 0)
            {
                XDetailEditor x = new XDetailEditor(selected, group, work, childDefs);
                x.ShowDialog();
                x.Dispose();
            }
        }
        private void cbxDataTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            string type = cbxDataTypes.SelectedItem.ToString();
            selected = (Datatype)Enum.Parse(typeof(Datatype), type);
        }
    }
}
