﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PaleHorseStudios.Shielder;

namespace Shielder_3._0.Views.Editors.Loadouts
{
    public partial class StringLoadout : Loadout
    {
        public StringLoadout(WorkView work)
            : base(Datatype.String, work.Package.Settings)
        {
            InitializeComponent();
        }
        public StringLoadout(Definition def, WorkView work)
            : base(Datatype.String, work.Package.Settings)
        {
            InitializeComponent();
            numCharLimit.Value = (decimal)def.CharLimit;
        }

        private void StringLoadout_Load(object sender, EventArgs e)
        {

        }
        public override string[] GetValues()
        {
            string[] values = new string[1];
            values[0] = numCharLimit.Value.ToString();
            return values;
        }
        public override List<string> ValidateItems(string oldName, List<Item> items, Definition def)
        {
            base.ValidateItems(oldName, items, def);
            List<string> changedItems = new List<string>();
            foreach (Item item in items)
            {
                if (item.ItemDataExists(def.Name))
                {
                    ItemData data = item.GetData(def.Name);
                    string value = data.Value;
                    if (value.Length > def.CharLimit)
                    {
                        value = value.Substring(0, def.CharLimit);
                        changedItems.Add(item.Parent.Text + "\\" + item.Text);
                        item.ForeColor = SettingsData.ChangedColor;
                    }
                    data.Value = value;
                    item.SaveData(data);
                }
            }
            return changedItems;
        }
        public override bool VerifyInput()
        {
            return true;
        }
    }
}
