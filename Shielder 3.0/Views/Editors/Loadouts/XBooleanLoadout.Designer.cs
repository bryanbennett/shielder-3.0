﻿namespace Shielder_3._0.Views.Editors.Loadouts
{
    partial class XBooleanLoadout
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rtbList = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // rtbList
            // 
            this.rtbList.Location = new System.Drawing.Point(41, 3);
            this.rtbList.Name = "rtbList";
            this.rtbList.Size = new System.Drawing.Size(328, 98);
            this.rtbList.TabIndex = 19;
            this.rtbList.Text = "<One element per line>";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "List:";
            // 
            // XBooleanLoadout
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.rtbList);
            this.Controls.Add(this.label2);
            this.Name = "XBooleanLoadout";
            this.Size = new System.Drawing.Size(377, 106);
            this.Load += new System.EventHandler(this.XBooleanLoadout_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtbList;
        private System.Windows.Forms.Label label2;
    }
}
