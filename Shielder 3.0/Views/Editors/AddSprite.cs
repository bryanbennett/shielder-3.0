﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PaleHorseStudios.Shielder;

namespace Shielder_3._0.Views.Editors
{
    public partial class AddSprite : Editor
    {
        private WorkView work;
        public AddSprite(WorkView work)
        {
            InitializeComponent();
            this.work = work;
        }
        public AddSprite()
        {
            InitializeComponent();
        }

        private void AddSprite_Load(object sender, EventArgs e)
        {

        }

        private void btnChoose_Click(object sender, EventArgs e)
        {
            DialogResult dr = openFileDialog.ShowDialog();

            if (dr == DialogResult.OK)
            {
                txtPath.Text = openFileDialog.SelectedPath;
            }
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            if (txtName.Text == String.Empty || txtName.Text == "")
            {
                OKMessage ok = new OKMessage("Enter A Group Name", "Group name cannot be blank.  Please enter a group name.");
                ok.ShowDialog();
                return;
            }
            if (!work.IsUniquePack(txtName.Text))
            {
                OKMessage ok = new OKMessage("Enter A Unique Group Name", "You must enter a unique group name.  The name cannot be the same as the package name, other group names, or item names.");
                ok.ShowDialog();
                return;
            }
            work.SetStatus("Importing SpritePack...");
            SpritePack sp = new SpritePack(txtName.Text, txtPath.Text);
            work.AddSpritePack(sp);
            work.SetStatus("Ready");
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {
            bool unique = work.IsUniquePack(txtName.Text);

            if (unique)
            {
                lblUnique.Visible = false;
            }
            else
            {
                lblUnique.Visible = true;
            }
        }
    }
}
