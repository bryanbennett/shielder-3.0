﻿using Shielder_3._0.Views.Editors.Loadouts;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PaleHorseStudios.Shielder;

namespace Shielder_3._0.Views.Editors
{
    public partial class XDetailEditor : DetailEditor
    {
        private Loadout loadout;
        private bool isChild = false;
        private List<Definition> childDefs;
        private SettingsData settingsData;

        public XDetailEditor()
        {
            InitializeComponent();
            
        }
        public XDetailEditor(Datatype type, Group group, WorkView work)
            :base("Detail Editor", group, work)
        {
            InitializeComponent();
            childDefs = new List<Definition>();
            settingsData = work.Package.Settings;
            CreateLoadout(type);
            pnlLoadout.Controls.Add(loadout);
        }
        public XDetailEditor(Datatype type, Group group, WorkView work, List<Definition> childDefs)
            :base("Detail Editor", group, work)
        {
            InitializeComponent();
            this.childDefs = childDefs;
            CreateLoadout(type);
            settingsData = work.Package.Settings;
            pnlLoadout.Controls.Add(loadout);
            isChild = true;
        }
        public XDetailEditor(Definition def, Group group, WorkView work)
            :base("Detail Editor", group, work, def)
        {
            InitializeComponent();
            childDefs = new List<Definition>();
            settingsData = work.Package.Settings;
            CreateLoadout(def);
            pnlLoadout.Controls.Add(loadout);
            txtName.Text = def.Name;
        }
        #region "Create Loadouts"
        private void CreateLoadout(Datatype type)
        {
            switch (type)
            {
                case Datatype.String:
                    loadout = new StringLoadout(WorkView);
                    break;
                case Datatype.Numeric:
                    loadout = new NumericLoadout(WorkView);
                    break;
                case Datatype.Image:
                    loadout = new ImageLoadout(WorkView);
                    break;
                case Datatype.XBoolean:
                    loadout = new XBooleanLoadout(WorkView);
                    break;
                case Datatype.Boolean:
                    loadout = new BooleanLoadout(ActiveGroup, WorkView, childDefs);
                    break;
                case Datatype.Reference:
                    loadout = new ReferenceLoadout(WorkView);
                    break;
            }
        }
        private void CreateLoadout(Definition def)
        {
            switch (def.Type)
            {
                case Datatype.String:
                    loadout = new StringLoadout(def, WorkView);
                    break;
                case Datatype.Numeric:
                    loadout = new NumericLoadout(def, WorkView);
                    break;
                case Datatype.Image:
                    loadout = new ImageLoadout(def, WorkView);
                    break;
                case Datatype.XBoolean:
                    loadout = new XBooleanLoadout(def, WorkView);
                    break;
                case Datatype.Boolean:
                    loadout = new BooleanLoadout(ActiveGroup, WorkView, def, childDefs);
                    break;
                case Datatype.Reference:
                    loadout = new ReferenceLoadout(def, WorkView);
                    break;
            }
        }
        #endregion

        private void XDetailEditor_Load(object sender, EventArgs e)
        {

        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (CheckDetailName() && loadout.VerifyInput())
            {
                Definition def = new Definition(txtName.Text, loadout.Type);

                foreach (Definition child in childDefs)
                {
                    string cName = child.Name;
                    string parent = def.Name;

                    if (cName.Equals(parent))
                    {
                        OKMessage ok = new OKMessage("Unique Names", "The parent and one or more children details share the same name.  Change the name of the parent to continue.");
                        ok.ShowDialog();
                        ok.Dispose();
                        return;
                    }
                }

                string[] values = loadout.GetValues();

                switch (loadout.Type)
                {
                    case Datatype.String:
                        def.CharLimit = Int32.Parse(values[0]);
                        break;
                    case Datatype.Numeric:
                        def.MinValue = Decimal.Parse(values[0]);
                        def.MaxValue = Decimal.Parse(values[1]);
                        def.DecimalPlaces = Int32.Parse(values[2]);
                        break;
                    case Datatype.Image:
                        def.SpritePack = values[0];
                        break;
                    case Datatype.XBoolean:
                        def.List = values;
                        break;
                    case Datatype.Boolean:
                        foreach (Definition child in childDefs)
                        {
                            child.ParentDefinition = def.Name;
                            ActiveGroup.AddDefinition(child);
                        }
                        break;
                    case Datatype.Reference:
                        def.Reference = values[0];
                        break;
                        //Add more Datatypes here.
                }
                if (isChild)
                {
                    //adds the definition to a list of child definitions
                    //to only be added if the parent definition is added.
                    childDefs.Add(def);
                    this.Close();
                }
                if (!NewDetail)
                {
                    List<Item> items = WorkView.GetItemsOfGroup(ActiveGroup);
                    List<string> changedItems = loadout.ValidateItems(Definition.Name, items, def);
                    if (changedItems.Count > 0)
                    {
                        ChangedItems(changedItems);
                    }
                    ActiveGroup.AddDefinition(Definition, def);
                }
                if (!isChild && NewDetail)
                {
                    ActiveGroup.AddDefinition(def);
                }
                WorkView.UpdateGroupSet();
                this.Close();                
            }
        }

        #region "Properties"
        /// <summary>
        /// Returns the settings data held in the Package node.
        /// </summary>
        public SettingsData SettingsData { get { return settingsData; } }
        #endregion
    }
}
