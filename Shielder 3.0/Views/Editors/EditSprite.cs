﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PaleHorseStudios.Shielder;

namespace Shielder_3._0.Views.Editors
{
    public partial class EditSprite : Editor
    {
        private WorkView work;
        private Dictionary<string, SpritePack> packs;
        private Dictionary<string, Image> images;
        private SpritePack activePack;
        public EditSprite()
        {
            InitializeComponent();
        }
        public EditSprite(WorkView work)
            :base("Edit Sprites")
        {
            InitializeComponent();
            this.work = work;
            packs = work.Package.SpritePacks;
            BuildComboBox();
            btnAdd.Visible = false;
            btnDelete.Visible = false;
        }

        private void BuildComboBox()
        {
            foreach (KeyValuePair<string, SpritePack> entry in packs)
            {
                cbxSpritePack.Items.Add(entry.Key);
            }
        }
        private void BuildImageBox()
        {
            images = activePack.ToDictionary();
            cbxImage.Items.Clear();
            foreach (KeyValuePair<string, Image> entry in images)
            {
                cbxImage.Items.Add(entry.Key);
            }
            pbxPreview.Visible = false;
        }

        private void EditSprite_Load(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            DialogResult dr = openFileDialog.ShowDialog();

            if (dr == DialogResult.OK)
            {
                string fileName = openFileDialog.SafeFileName;
                string path = openFileDialog.FileName;
                try
                {
                    Image img = Image.FromFile(path);
                    byte[] byteImg = SpritePack.ImageToByte(img);
                    bool added = activePack.AddByteArray(fileName, byteImg);

                    if (added)
                    {
                        OKMessage ok = new OKMessage("Image Added", "The image " + fileName + " has been added to the " + activePack.Name + " SpritePack.");
                        ok.ShowDialog();
                        ok.Dispose();
                        BuildImageBox();
                    }
                    else
                    {
                        OKMessage ok = new OKMessage("Image Add Failure", "The image " + fileName + " could not be added to the " + activePack.Name + " SpritePack.  Ensure that the name of the image is unique within the SpritePack.");
                        ok.ShowDialog();
                        ok.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    OKMessage ok = new OKMessage("Improper Image Format", "The file you selected is not a supported image type.\n\nInfo:\n\n" + ex.ToString());
                    ok.ShowDialog();
                    ok.Dispose();
                }
            }


        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            string key = (string)cbxImage.SelectedItem;

            if (activePack.ContainsImage(key))
            {
                activePack.RemoveImage(key);
                OKMessage ok = new OKMessage("Image Deletion", "The image ["+ key + "] has been removed from the active SpritePack.");
                ok.ShowDialog();
                ok.Dispose();
                BuildImageBox();
            }
        }
        private void cbxSpritePack_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbxSpritePack.SelectedIndex >= 0)
            {
                activePack = packs[(string)cbxSpritePack.SelectedItem];
                BuildImageBox();
                pbxPreview.Image = null;
                btnAdd.Visible = true;
                btnDelete.Visible = false;
            }
            else
            {
                cbxImage.Items.Clear();
                btnAdd.Visible = false;
            }
        }

        private void cbxImage_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbxImage.SelectedIndex >= 0)
            {
                Image img = images[(string)cbxImage.SelectedItem];
                pbxPreview.Image = img;
                pbxPreview.Visible = true;
                btnDelete.Visible = true;
            }
            else
            {
                pbxPreview.Visible = false;
                btnDelete.Visible = false;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
