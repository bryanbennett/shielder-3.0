﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PaleHorseStudios.Shielder;

namespace Shielder_3._0.Views
{
    public partial class ItemEditor : Editor
    {
        private Item item;
        private WorkView work;
        private Group parent;
        private bool newItem;

        public ItemEditor(WorkView work, Group parent)
            : base("Item Editor")
        {
            InitializeComponent();
            this.work = work;
            this.parent = parent;
            newItem = true;
        }
        public ItemEditor(WorkView work, Item item)
            : base("Item Editor")
        {
            InitializeComponent();
            this.item = item;
            this.work = work;
            this.parent = (Group)item.Parent;
        }
        public ItemEditor()
        {
            InitializeComponent();
        }

        private void ItemEditor_Load(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            string name = txtName.Text;

            if (txtName.Text == String.Empty || txtName.Text == "")
            {
                OKMessage ok = new OKMessage("Enter an Item Name", "Item name cannot be blank.  Please enter an Item name.");
                ok.ShowDialog();
                return;
            }
            if (!work.IsUniqueName(txtName.Text))
            {
                OKMessage ok = new OKMessage("Enter A Unique Item Name", "You must enter a unique Item name.  The name cannot be the same as the package name, group names, or other item names.");
                ok.ShowDialog();
                return;
            }

            if (newItem)
            {
                Item item = new Item(name, parent);
                item.ForeColor = work.Settings.ItemColor;
                work.ExpandTree();
                item.ContextMenuStrip = work.ItemContext;
                work.UpdateCounts();
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {
            bool unique = work.IsUniqueName(txtName.Text);

            if (unique)
            {
                lblUnique.Visible = false;
            }
            else
            {
                lblUnique.Visible = true;
            }
        }
    }
}
