﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shielder_3._0.Views
{
    public partial class Editor : Form
    {
        private bool isDragging;
        private Point dragAnchor;
        public Editor(string title)
        {
            InitializeComponent();
            lblTitle.Text = title;
        }
        public Editor()
        {
            InitializeComponent();
        }

        private void Editor_Load(object sender, EventArgs e)
        {

        }
        #region "Title Bar and Drag"
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void pnlTitleBar_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && !isDragging)
            {
                isDragging = true;
                dragAnchor = new Point(e.X, e.Y);
            }
        }
        private void pnlTitleBar_MouseMove(object sender, MouseEventArgs e)
        {
            if (isDragging)
            {
                Bounds = new Rectangle(Bounds.X + e.X - dragAnchor.X, Bounds.Y + e.Y - dragAnchor.Y, Bounds.Width, Bounds.Height);
            }
        }

        private void pnlTitleBar_MouseUp(object sender, MouseEventArgs e)
        {
            isDragging = false;
        }
        #endregion
        #region "Properties"


        #endregion
    }
}
