﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PaleHorseStudios.Shielder;

namespace Shielder_3._0.Views
{
    public partial class NewPackageEditor : Editor
    {
        private StartView start;
        private string id;
        public NewPackageEditor(StartView start, string newPackageID)
            :base("New Package")
        {
            InitializeComponent();
            this.start = start;
            id = newPackageID;
        }
        public NewPackageEditor()
        {
            InitializeComponent();
        }

        private void PackageEditor_Load(object sender, EventArgs e)
        {
            ActiveControl = txtName;
            txtName.Focus();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string name = txtName.Text;

            if (name.Equals(""))
            {
                OKMessage ok = new OKMessage("Invalid Package Name", "You must enter a valid package name.");
                ok.ShowDialog();
                return;
            }

            Package p = new Package(name, id);
            start.SetPackage(p);
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
