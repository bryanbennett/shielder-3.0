﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PaleHorseStudios.Shielder;

namespace Shielder_3._0.Views
{
    public partial class GroupEditor : Editor
    {
        private Group group;
        private List<Group> groups;
        private bool newGroup;
        private WorkView work;
        private string super;

        public GroupEditor(WorkView work)
            : base("Group Editor")
        {
            InitializeComponent();
            newGroup = true;
            this.work = work;
            Init();
        }
        public GroupEditor(WorkView work, Group group)
            : base("Group Editor")
        {
            InitializeComponent();
            this.work = work;
            Init(group.Text);
            super = group.Text;
        }
        public GroupEditor()
        {
            InitializeComponent();
        }
        private void Init()
        {
            groups = work.GetGroups();

            cbxGroups.Items.Add("None");
            foreach (Group group in groups)
            {
                cbxGroups.Items.Add(group.Text);
            }
            cbxGroups.SelectedIndex = 0;
            this.ActiveControl = txtName;
        }
        private void Init(string setGroup)
        {
            cbxGroups.Items.Add(setGroup);
            cbxGroups.SelectedIndex = 0;
            cbxGroups.Enabled = false;
        }

        private void GroupEditor_Load(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (txtName.Text == String.Empty || txtName.Text == "")
            {
                OKMessage ok = new OKMessage("Enter A Group Name", "Group name cannot be blank.  Please enter a group name.");
                ok.ShowDialog();
                return;
            }
            if (!work.IsUniqueName(txtName.Text))
            {
                OKMessage ok = new OKMessage("Enter A Unique Group Name", "You must enter a unique group name.  The name cannot be the same as the package name, other group names, or item names.");
                ok.ShowDialog();
                return;
            }


            if (newGroup)
            {
                string select = cbxGroups.SelectedItem.ToString();
                if (cbxGroups.SelectedIndex > 0)
                {
                    Group superGroup = groups[cbxGroups.SelectedIndex - 1];
                    group = new Group(txtName.Text, superGroup);
                    group.ForeColor = superGroup.ForeColor;
                }
                else
                {
                    Package package = work.Package;
                    group = new Group(txtName.Text, package);
                    group.ForeColor = package.Settings.GroupColor;
                }
            }
            else
            {
                Group superGroup = work.GetGroup(super);
                group = new Group(txtName.Text, superGroup);
                group.ForeColor = superGroup.ForeColor;
            }
            work.ExpandTree();
            group.ContextMenuStrip = work.GroupContext;
            work.UpdateCounts();
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {
            bool unique = work.IsUniqueName(txtName.Text);

            if (unique)
            {
                lblUnique.Visible = false;
            }
            else
            {
                lblUnique.Visible = true;
            }
        }
    }
}
