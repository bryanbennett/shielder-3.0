﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PaleHorseStudios.Shielder;

namespace Shielder_3._0.Views
{
    public partial class DetailEditor : Editor
    {
        private WorkView work;
        private Group group;
        private bool newDetail;
        private Definition def;

        public DetailEditor(string title, Group group, WorkView work)
            :base("Detail Editor")
        {
            InitializeComponent();
            this.group = group;
            this.work = work;
            newDetail = true;
        }
        public DetailEditor(string title, Group group, WorkView work, Definition def)
            : base("Detail Editor")
        {
            InitializeComponent();
            this.group = group;
            this.work = work;
            this.def = def;
        }
        public DetailEditor()
        {
            InitializeComponent();
        }

        private void DetailEditor_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Returns whether or not the detail name is valid.
        /// </summary>
        /// <returns></returns>
        protected bool CheckDetailName()
        {
            string name = txtName.Text;

            if (txtName.Text.Equals(String.Empty) || txtName.Text.Equals(""))
            {
                OKMessage ok = new OKMessage("Enter a Detail Name", "Detail name cannot be blank.  Please enter a Detail name.");
                ok.ShowDialog();
                return false;
            }
            if (NewDetail && !group.IsUniqueDefinitionName(txtName.Text))
            {
                OKMessage ok = new OKMessage("Unique Detail Name", "The Detail name entered is not unique.  Please enter a unique Detail name.");
                ok.ShowDialog();
                return false;
            }
            return true;
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {
            bool unique = group.IsUniqueDefinitionName(txtName.Text);

            if (unique || txtName.Text.Equals(Definition.Name))
            {
                lblUnique.Visible = false;
            }
            else
            {
                lblUnique.Visible = true;
            }
        }
        private void ShowMessage(string title, string message)
        {
            OKMessage ok = new OKMessage(title, message);
            ok.ShowDialog();
            ok.Dispose();
        }
        /// <summary>
        /// Displays an OKMessage about items who's values were affected by a change.
        /// </summary>
        /// <param name="items"></param>
        public void ChangedItems(List<string> items)
        {
            string title = "Value Changed";
            string message = "The following items have had their values changed:\n\n";

            foreach (string entry in items)
            {
                message += entry + "\n";
            }
            ShowMessage(title, message);
        }
        #region "Properties"

        /// <summary>
        /// Gets the definition associated with this editor.
        /// </summary>
        protected Definition Definition { get { return def; } }

        /// <summary>
        /// Returns whether or not this editor is working with a new detail or an already-existing detail.
        /// </summary>
        protected bool NewDetail { get { return newDetail; } }

        /// <summary>
        /// Returns the current working group.
        /// </summary>
        protected Group ActiveGroup { get { return group; } }

        /// <summary>
        /// Returns the work view associated with this editor.
        /// </summary>
        protected WorkView WorkView { get { return work; } }
        #endregion

        private void btnAdd_Click(object sender, EventArgs e)
        {

        }
    }
}
