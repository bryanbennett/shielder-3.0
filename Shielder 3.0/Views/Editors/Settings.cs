﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PaleHorseStudios.Shielder;

namespace Shielder_3._0.Views.Editors
{
    public partial class Settings : Editor
    {
        private SettingsData data;

        public Settings(SettingsData data)
            :base("Settings")
        {
            InitializeComponent();
            this.data = data;
            lblPackage.ForeColor = data.PackageColor;
            lblGroup.ForeColor = data.GroupColor;
            lblItem.ForeColor = data.ItemColor;
        }

        private void Settings_Load(object sender, EventArgs e)
        {

        }

        private void lblPackage_Click(object sender, EventArgs e)
        {
            DialogResult result = colorDialog1.ShowDialog();

            if (result == DialogResult.OK)
            {
                lblPackage.ForeColor = colorDialog1.Color;
                data.PackageColor = lblPackage.ForeColor;
            }
        }

        private void lblGroup_Click(object sender, EventArgs e)
        {
            DialogResult result = colorDialog1.ShowDialog();

            if (result == DialogResult.OK)
            {
                lblGroup.ForeColor = colorDialog1.Color;
                data.GroupColor = lblGroup.ForeColor;
            }
        }

        private void lblItem_Click(object sender, EventArgs e)
        {
            DialogResult result = colorDialog1.ShowDialog();

            if (result == DialogResult.OK)
            {
                lblItem.ForeColor = colorDialog1.Color;
                data.ItemColor = lblItem.ForeColor;
            }
        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            data.Update();
            this.Close();
        }

        private void lblHighlight_Click(object sender, EventArgs e)
        {
            DialogResult result = colorDialog1.ShowDialog();

            if (result == DialogResult.OK)
            {
                Color c = colorDialog1.Color;
                Color i = lblItem.ForeColor;
                Color g = lblGroup.ForeColor;
                Color p = lblPackage.ForeColor;
                if (c == i || c == g || c == p)
                {
                    OKMessage ok = new OKMessage("Color Ambiguity", "You cannot set the highlight color to the same color as Items, Groups, or Packages.  Please choose a different color.");
                    ok.ShowDialog();
                    return;
                }
                else
                {
                    lblHighlight.ForeColor = colorDialog1.Color;
                    data.ChangedColor = lblHighlight.ForeColor;
                }
            }
        }
    }
}
