﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Shielder_3._0.Details;
using PaleHorseStudios.Shielder;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using Shielder_3._0.Views.Editors;
using Shielder_3._0.Data;
using Microsoft.Win32;

namespace Shielder_3._0.Views
{
    public partial class WorkView : BaseView
    {
        private Package package;
        private List<TreeNode> searchNodes;
        private int searchIndex = 0;
        private ShielderNode selectedNode;
        private bool hasSelection;

        #region "Constructors"
        public WorkView(Main main)
            :base(main)
        {
            InitializeComponent();
            searchNodes = new List<TreeNode>();
        }
        private void WorkView_Load(object sender, EventArgs e)
        {

        }
        #endregion
        #region "Initialization"
        public void GenerateTree(Package package)
        {
            packageTree.Nodes.Clear();
            packageTree.Nodes.Add(package);
            packageTree.ExpandAll();
            this.package = package;
            RecursiveMenuSet(packageTree.Nodes);
            Main.UpdateCounts(package);
        }
        /// <summary>
        /// Sets the context menu of all nodes.
        /// </summary>
        private void RecursiveMenuSet(TreeNodeCollection nodes)
        {
            foreach (TreeNode node in nodes)
            {
                ShielderNode sNode = (ShielderNode)node;
                Layer val = sNode.Layer;

                switch (val)
                {
                    case Layer.Package:
                        sNode.ContextMenuStrip = packageContext;
                        break;
                    case Layer.Group:
                        sNode.ContextMenuStrip = groupContext;
                        break;
                    case Layer.Item:
                        sNode.ContextMenuStrip = itemContext;
                        break;
                }
                RecursiveMenuSet(sNode.Nodes);
            }
        }
        #endregion
        #region "TreeSearch"
        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            string text = txtSearch.Text;
            if (text.Equals(String.Empty))
            {
                return;
            }
            text = text.ToLower();

            //checks to make sure a package actually exists.  Could be redundant.
            if (!(packageTree.Nodes.Count > 0))
            {
                return;
            }
            StartSearch(text);
        }
        /// <summary>
        /// Starts the search for a node and initializes required conditions.
        /// </summary>
        /// <param name="text"></param>
        private void StartSearch(string text)
        {
            searchNodes.Clear();
            searchIndex = 0;
            SearchForNode(text, packageTree.Nodes[0]);
            if (searchNodes.Count > 0)
            {
                packageTree.SelectedNode = searchNodes[0];
            }
        }
        /// <summary>
        /// Recursively searches for the node that starts with the
        /// search text.  If one is found, the node is selected and
        /// the recursion stops.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="node"></param>
        private void SearchForNode(string text, TreeNode node)
        {
            string name = node.Text;
            name = name.ToLower();

            if (name.StartsWith(text) && node.Level > 0)
            {
                searchNodes.Add(node);
                if (node.Nodes.Count > 0)
                {
                    foreach (TreeNode entry in node.Nodes)
                    {
                        SearchForNode(text, entry);
                    }
                }
            }
            else
            {
                if (node.Nodes.Count > 0)
                {
                    foreach (TreeNode entry in node.Nodes)
                    {
                        SearchForNode(text, entry);
                    }
                }
            }
            return;
        }
        private ShielderNode SearchForShielderNode(string text, ShielderNode node)
        {
            string name = node.Text;
            if (name.Equals(text))
            {
                return node;
            }
            else
            {
                if (node.Nodes.Count > 0)
                {
                    foreach (ShielderNode entry in node.Nodes)
                    {
                        ShielderNode sNode = SearchForShielderNode(text, entry);
                        if (sNode != null)
                        {
                            return sNode;
                        }
                    }
                }
            }
            return null;
        }
        private void txtSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (searchNodes.Count > 0)
            {
                int count = searchNodes.Count;
                if (e.KeyCode == Keys.PageDown)
                {
                    searchIndex += 1;
                    if (searchIndex >= count)
                    {
                        searchIndex = 0;
                    }
                    packageTree.SelectedNode = searchNodes[searchIndex];
                }
                else if (e.KeyCode == Keys.PageUp)
                {
                    searchIndex -= 1;
                    if (searchIndex < 0)
                    {
                        searchIndex = 0;
                    }
                    packageTree.SelectedNode = searchNodes[searchIndex];
                }
            }
        }
        /// <summary>
        /// Returns whether or not the specified name of a node is unique in the tree.
        /// This is not case-sensitive, so atlas --> Atlas will return false.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool IsUniqueName(string name)
        {
            string temp = name.ToLower();
            return IsUniqueName(temp, package);
        }
        private bool IsUniqueName(string name, ShielderNode node)
        {
            string lower = node.Text.ToLower();
            if (lower.Equals(name))
            {
                return false;
            }
            else if (node.Nodes.Count > 0)
            {
                foreach (ShielderNode sNode in node.Nodes)
                {
                    bool isUnique = IsUniqueName(name, sNode);
                    if (!isUnique)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        #endregion
        #region "Detail Generation"
        /// <summary>
        /// Method that loads all the details for the item and sets the values of all the details appropriately.  Re-Ordering is disabled.
        /// </summary>
        /// <param name="item"></param>
        private void LoadItemSet(Item item)
        {
            pnlDetails.Controls.Clear();
            Group parent = (Group) item.Parent;
            List<Definition> defs = parent.GetDefinitions();
            List<Detail> childDetails = new List<Detail>();

            foreach (Definition def in defs)
            {
                Datatype type = def.Type;
                Detail det = null;
                try
                {
                    switch (type)
                    {
                        case Datatype.Boolean:
                            det = new BooleanDetail(def, this);
                            break;
                        case Datatype.XBoolean:
                            det = new XBooleanDetail(def, this);
                            break;
                        case Datatype.Image:
                            det = new ImageDetail(def, this);
                            break;
                        case Datatype.String:
                            det = new StringDetail(def, this);
                            break;
                        case Datatype.Numeric:
                            det = new NumericDetail(def, this);
                            break;
                        case Datatype.Reference:
                            det = new ReferenceDetail(def, this);
                            break;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

                det.AllowModification = false;

                if (item.ItemDataExists(det.DetailName))
                {
                    det.SetItemData(item.GetData(det.DetailName));
                }
                if (def.IsChild)
                {
                    childDetails.Add(det);
                }
                else
                {
                    pnlDetails.Controls.Add(det);
                }
            }
            try
            {
                foreach (Detail det in childDetails)
                {
                    string parent2 = det.Definition.ParentDefinition;
                    foreach (Detail det2 in pnlDetails.Controls)
                    {
                        if (parent2.Equals(det2.DetailName))
                        {
                            BooleanDetail temp = (BooleanDetail)det2;
                            temp.AddChild(det, false);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        /// <summary>
        /// Updates the rendered details when a definition has changed.
        /// </summary>
        public void UpdateGroupSet()
        {
            LoadGroupSet((Group)SelectedNode);
        }
        /// <summary>
        /// Method that loads all the default Details for its items.  Re-ordering is enabled.
        /// </summary>
        /// <param name="group"></param>
        private void LoadGroupSet(Group group)
        {
            pnlDetails.Controls.Clear();
            List<Definition> defs = group.Definitions;
            List<Detail> childDetails = new List<Detail>();

            foreach (Definition def in defs)
            {
                Datatype type = def.Type;
                Detail det = null;
                try
                {
                    switch (type)
                    {
                        case Datatype.Boolean:
                            det = new BooleanDetail(def, this);
                            break;
                        case Datatype.XBoolean:
                            det = new XBooleanDetail(def, this);
                            break;
                        case Datatype.Image:
                            det = new ImageDetail(def, this);
                            break;
                        case Datatype.String:
                            det = new StringDetail(def, this);
                            break;
                        case Datatype.Numeric:
                            det = new NumericDetail(def, this);
                            break;
                        case Datatype.Reference:
                            det = new ReferenceDetail(def, this);
                            break;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

                det.AllowModification = true;
                if (def.IsChild)
                {
                    childDetails.Add(det);
                }
                else
                {
                    pnlDetails.Controls.Add(det);
                }
            }
            try
            {
                foreach (Detail det in childDetails)
                {
                    string parent = det.Definition.ParentDefinition;
                    foreach (Detail det2 in pnlDetails.Controls)
                    {
                        if (parent.Equals(det2.DetailName))
                        {
                            BooleanDetail temp = (BooleanDetail)det2;
                            temp.AddChild(det, true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }
        #endregion
        #region "General Operations"

        /// <summary>
        /// Sets the status text of the ready bar.
        /// </summary>
        /// <param name="status"></param>
        public void SetStatus(string status)
        {
            Main.SetStatus(status);
        }
        /// <summary>
        /// Updates the amount of groups and items found in the lower right hand corner of the window.
        /// </summary>
        public void UpdateCounts()
        {
            Main.UpdateCounts(package);
            Main.ShowSave(true);
        }
        /// <summary>
        /// Returns whether or not the package already has a spritepack with the specified name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool IsUniquePack(string name)
        {
            return !package.ContainsSpritePack(name);
        }
        /// <summary>
        /// Adds the definition to the currently selected group node.
        /// </summary>
        /// <param name="def"></param>
        public void AddDefinition(Definition def)
        {
            Group group = (Group)selectedNode;
            group.AddDefinition(def);
            Main.ShowSave(true);
        }
        private void AddSpritePack()
        {
            AddSprite addSp = new AddSprite(this);
            addSp.ShowDialog();
        }
        private void packageTree_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                packageTree.SelectedNode = e.Node;
            }
        }
        public void AddSpritePack(SpritePack sp)
        {
            package.AddSpritePack(sp);
            Main.ShowSave(true);
        }

        /// <summary>
        /// Expands the TreeView.
        /// </summary>
        public void ExpandTree()
        {
            packageTree.ExpandAll();
        }
        /// <summary>
        /// Re-orders the details according to the passed in value.  Integer value of 1 indicates the control should
        /// go down in the list, and -1 indicates the control should go up.
        /// </summary>
        /// <param name="upOrDown"></param>
        public void ReOrder(int upOrDown, Detail detail)
        {
            ControlCollection controls = pnlDetails.Controls;
            if (!detail.Child)
            {
                int index = controls.IndexOf(detail);

                if (index + upOrDown < controls.Count)
                {
                    controls.SetChildIndex(detail, index + upOrDown);
                }
                else
                {
                    controls.SetChildIndex(detail, 0);
                }
            }

            Group parent = (Group)selectedNode;

            List<Definition> defs = parent.Definitions;
            List<Definition> childDefs = new List<Definition>();

            foreach (Definition def in defs)
            {
                if (!def.ParentDefinition.Equals(""))
                {
                    childDefs.Add(def);
                }
            }
            defs.Clear();

            foreach (Control entry in controls)
            {
                Detail det = (Detail)entry;

                defs.Add(det.Definition);
            }
            foreach (Definition def in childDefs)
            {
                defs.Add(def);
            }
            Main.ShowSave(true);
        }
        /// <summary>
        /// Removes the detail from the detail panel and deletes the definition from the group.
        /// </summary>
        /// <param name="detail"></param>
        public void DeleteDefinition(Detail detail)
        {
            YESNOMessage yesno = new YESNOMessage("Deletion", "Are you sure you wish to delete this detail?  This will result in loss of information in items that belong to this group.");

            if (yesno.ShowDialog() == DialogResult.Yes)
            {
                Definition def = detail.Definition;
                pnlDetails.Controls.Remove(detail);
                Group group = (Group)selectedNode;
                List<Item> items = GetItemsOfGroup(group);
                group.RemoveDefinition(def, items);
                Main.ShowSave(true);
                LoadGroupSet(group);
            }
        }
        private void packageTree_AfterSelect(object sender, TreeViewEventArgs e)
        {
            SaveCurrentNode();
            ShielderNode node = (ShielderNode)packageTree.SelectedNode;
            selectedNode = node;
            hasSelection = true;
            lblNodeTitle.Text = node.FullPath;
            if (node.Layer == Layer.Package)
            {
                node.ForeColor = Settings.PackageColor;
                pnlDetails.Controls.Clear();
            }
            if (node.Layer == Layer.Group)
            {
                node.ForeColor = Settings.GroupColor;
                Group gNode = (Group)node;
                LoadGroupSet(gNode);
            }
            if (node.Layer == Layer.Item)
            {
                node.ForeColor = Settings.ItemColor;
                Item iNode = (Item)node;
                LoadItemSet(iNode);
            }
        }
        #endregion
        #region "Collection/Node Getting"
        /// <summary>
        /// Returns the specified group.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public Group GetGroup(string name)
        {
            return (Group)SearchForShielderNode(name, package);
        }
        /// <summary>
        /// Returns all groups in the package.
        /// </summary>
        /// <returns></returns>
        public List<Group> GetGroups()
        {
            List<Group> groups = new List<Group>();
            GetGroups(groups, packageTree.Nodes[0]);
            return groups;
        }
        /// <summary>
        /// Recursively called to generate the list of Group nodes.
        /// </summary>
        /// <param name="list"></param>
        /// <param name="node"></param>
        private void GetGroups(List<Group> list, TreeNode node)
        {
            ShielderNode sNode = (ShielderNode)node;
            if (sNode.Layer == Layer.Group)
            {
                Group gNode = (Group)sNode;
                list.Add(gNode);
            }
            foreach (TreeNode entry in node.Nodes)
            {
                GetGroups(list, entry);
            }

        }
        /// <summary>
        /// Returns all items under the specified group.
        /// </summary>
        /// <returns></returns>
        public List<Item> GetItemsOfGroup(Group group)
        {
            List<Item> items = new List<Item>();
            string name = group.Text;
            GetItemsOfGroup(items, group);
            return items;
        }
        /// <summary>
        /// Recursively called to generate the list of Item nodes.
        /// </summary>
        private void GetItemsOfGroup(List<Item> items, Group group)
        {
            TreeNodeCollection nodes = group.Nodes;

            foreach (TreeNode node in nodes)
            {
                ShielderNode sNode = (ShielderNode)node;
                if (sNode.Layer == Layer.Item)
                {
                    items.Add((Item)sNode);
                }
                else if (sNode.Layer == Layer.Group)
                {
                    GetItemsOfGroup(items, (Group)sNode);
                }
            }
        }
        /// <summary>
        /// Returns a list of items that contain the specified definition name of a ReferenceDetail
        /// with the specified reference as a value.
        /// </summary>
        /// <param name="defName"></param>
        /// <param name="reference"></param>
        /// <returns></returns>
        public List<Item> GetItemsWithReference(string reference)
        {
            List<Item> items = new List<Item>();
            GetItemsWithReference(reference, package, items);
            return items;
        }
        private void GetItemsWithReference(string reference, ShielderNode node, List<Item> items)
        {
            if (node.Layer != Layer.Item)
            {
                int count = node.Nodes.Count;
                for (int i = 0; i < count; i++)
                {
                    GetItemsWithReference(reference, (ShielderNode)node.Nodes[i], items);
                }
            }
            else
            {
                Item sNode = (Item)node;
                Dictionary<string, ItemData> datum = sNode.Datum;
                Group g = (Group)sNode.Parent;

                foreach (Definition def in g.Definitions)
                {
                    if (def.Type == Datatype.Reference && datum.ContainsKey(def.Name))
                    {
                        ItemData data = datum[def.Name];
                        if (data.Value.Equals(reference))
                        {
                            items.Add(sNode);
                        }
                    }
                }

            }
        }
        #endregion
        #region "Properties"
        /// <summary>
        /// Returns the currently selected node.
        /// </summary>
        public ShielderNode SelectedNode { get { return selectedNode; } }

        /// <summary>
        /// Returns the active package.
        /// </summary>
        public Package Package { get { return package; } }

        /// <summary>
        /// Returns the context menu associated with groups.
        /// </summary>
        public ContextMenuStrip GroupContext { get { return groupContext; } }

        /// <summary>
        /// Returns the context menu associated with packages.
        /// </summary>
        public ContextMenuStrip PackageContext { get { return packageContext; } }

        /// <summary>
        /// Returns the context menu associated with items.
        /// </summary>
        public ContextMenuStrip ItemContext { get { return itemContext; } }

        /// <summary>
        /// Gets the SettingsData associated with this package.
        /// </summary>
        public SettingsData Settings { get { return package.Settings; } }
        #endregion
        #region "Context Menus For Groups"
        private void stripAddString_Click(object sender, EventArgs e)
        {
            ShowEditor(Datatype.String);
        }
        private void stripAddBoolean_Click(object sender, EventArgs e)
        {
            ShowEditor(Datatype.Boolean);
        }

        private void stripAddReference_Click(object sender, EventArgs e)
        {
            ShowEditor(Datatype.Reference);
        }

        private void stripAddImage_Click(object sender, EventArgs e)
        {
            ShowEditor(Datatype.Image);
        }

        private void stripAddNumerical_Click(object sender, EventArgs e)
        {
            ShowEditor(Datatype.Numeric);
        }

        private void stripAddXBool_Click(object sender, EventArgs e)
        {
            ShowEditor(Datatype.XBoolean);
        }
        private void stripNewItem_Click(object sender, EventArgs e)
        {
            Group group = (Group)selectedNode;
            ItemEditor ie = new ItemEditor(this, group);
            ie.ShowDialog();
        }
        private void ShowEditor(Datatype type)
        {
            Group group = (Group)selectedNode;
            XDetailEditor x = new XDetailEditor(type, group, this);
            x.ShowDialog();
            LoadGroupSet(group);
        }
        private void ShowEditor(Definition def)
        {
            Group group = (Group)selectedNode;
            XDetailEditor x = new XDetailEditor(def, group, this);
            x.ShowDialog();
        }
        private void stripAddSub_Click(object sender, EventArgs e)
        {
            GroupEditor ge = new GroupEditor(this, (Group)selectedNode);
            ge.ShowDialog();
            ge.Dispose();
        }
        private void stripDeleteGroup_Click(object sender, EventArgs e)
        {
            YESNOMessage yesno = new YESNOMessage("Deletion", "Are you sure you wish to delete this group?  All subgroups and items under this group will be lost.");

            if (yesno.ShowDialog() == DialogResult.Yes)
            {
                Group group = (Group)selectedNode;

                List<Group> groups = GetGroups();

                foreach (Group entry in groups)
                {
                    List<Definition> defs = entry.Definitions;
                    int count = defs.Count;

                    for (int i = 0; i < count; i++)
                    {
                        Definition def = defs[i];
                        if (def.Reference.Equals(group.Text))
                        {
                            entry.RemoveDefinition(def, GetItemsOfGroup(entry));
                        }
                    }
                }

                package.Nodes.Remove(group);
                UpdateCounts();
            }
        }
        #endregion
        #region "Context Menus for Packages"
        private void stripNewGroup_Click(object sender, EventArgs e)
        {
            GroupEditor ge = new GroupEditor(this);
            ge.ShowDialog();
            ge.Focus();
        }
        private void stripAddPack_Click(object sender, EventArgs e)
        {
            AddSpritePack();
        }
        #endregion
        #region "Context Menus for Items"
        private void stripDeleteItem_Click(object sender, EventArgs e)
        {
            YESNOMessage yesno = new YESNOMessage("Item Deletion", "Are you sure you wish to delete this item?");
            DialogResult dr = yesno.ShowDialog();

            if (dr == DialogResult.Yes)
            {
                if (selectedNode.Layer == Layer.Item)
                {
                    List<Item> changedItems = GetItemsWithReference(selectedNode.Text);
                    string message = "The following items were affected by the deletion of the item:\n\n";

                    foreach (Item item in changedItems)
                    {
                        item.ForeColor = Settings.ChangedColor;
                        message += item.Text + "\n";
                    }
                    OKMessage ok = new OKMessage("Item References Removed.", message);
                    ok.ShowDialog();
                    selectedNode.Remove();
                    UpdateCounts();
                }
            }
        }
        #endregion
        #region "Saving"
        /// <summary>
        /// Saves the currently selected node.
        /// </summary>
        private void SaveCurrentNode()
        {
            if (hasSelection)
            {
                if (selectedNode.Layer == Layer.Item)
                {
                    Item item = (Item)selectedNode;
                    ControlCollection details = pnlDetails.Controls;

                    foreach (Control entry in details)
                    {
                        Detail det = (Detail)entry;
                        item.SaveData(det.ItemData);
                        if (det.Datatype == Datatype.Boolean)
                        {
                            BooleanDetail bDet = (BooleanDetail)det;
                            if (bDet.HasChildDefinitions)
                            {
                                List<ItemData> datum = bDet.ChildData();

                                foreach (ItemData data in datum)
                                {
                                    item.SaveData(data);
                                }
                            }

                        }
                    }
                }
                if (selectedNode.Layer == Layer.Group)
                {
                    Group group = (Group)selectedNode;
                    ControlCollection details = pnlDetails.Controls;

                    foreach (Control entry in details)
                    {
                        Detail det = (Detail)entry;
                        group.SaveDefault(det.ItemData);
                    }
                }
                if (selectedNode.Layer == Layer.Package)
                {
                    pnlDetails.Controls.Clear();
                }
            }
        }
        private void savePackageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveCurrentNode();
            if (!package.HasPath)
            {
                ChooseSaveLocation();
            }
            else
            {
                Save();
            }
        }
        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveCurrentNode();
            ChooseSaveLocation();
        }
        private bool ChooseSaveLocation()
        {
            DialogResult result = saveFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                string path = saveFileDialog.FileName;
                package.FilePath = path;
                Save();
                return true;
            }
            return false;
        }
        private void Save()
        {
            Stream streamer = File.Create(package.FilePath);
            BinaryFormatter f = new BinaryFormatter();
            f.Serialize(streamer, package);
            streamer.Close();
            Main.ShowSave(false);
            OKMessage ok = new OKMessage("Save", "The package was saved to " + package.FilePath + ".");
            ok.ShowDialog();

            ShielderSettings ss = Main.Settings;
            bool exists = ss.PackageExists(package.ID);
            if (!exists)
            {
                ss.AddPackage(package.ID, package.FilePath);
            }
        }
        #endregion
        #region "WorkView Menu Items"
        private void openPackageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                string fileName = openFileDialog.FileName;
                if (File.Exists(fileName))
                {
                    Stream streamer = File.OpenRead(fileName);
                    BinaryFormatter ds = new BinaryFormatter();
                    Package package = (Package)ds.Deserialize(streamer);
                    streamer.Close();
                    Main.NewPackage(package);
                }
            }
        }
        private void newPackageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            YESNOMessage yesno = new YESNOMessage("Save Package", "Do you wish to save the current package?");
            DialogResult dr = yesno.ShowDialog();

            if (dr == DialogResult.Yes)
            {
                Save();
            }
            StartView start = (StartView)Main.GetView(ViewState.Start);
            NewPackageEditor npe = new NewPackageEditor(start, System.Guid.NewGuid().ToString());
            npe.ShowDialog();
            npe.Dispose();
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Settings s = new Settings(package.Settings);
            s.ShowDialog();
        }
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Main.Close();
        }
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox ab = new AboutBox();
            ab.ShowDialog();
            ab.Dispose();
        }
        private void addSpriteStripMenuItem_Click(object sender, EventArgs e)
        {
            AddSpritePack();
        }

        private void editSpritesStripMenuItem_Click(object sender, EventArgs e)
        {
            EditSprite es = new EditSprite(this);
            es.ShowDialog();
            es.Dispose();
        }
        private void howToToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var adobePath = Registry.GetValue(@"HKEY_CLASSES_ROOT\Software\Adobe\Acrobat\Exe", string.Empty, string.Empty);

            if (adobePath != null)
            {
                HowTo ht = new HowTo();
                ht.Show();
                ht.TopMost = true;
            }
            else
            {
                OKMessage ok = new OKMessage("Adobe Missing", "You cannot view the help file until you download Adobe Acrabat Reader.");
                ok.ShowDialog();
                ok.Dispose();
            }
        }
        #endregion

        private void toolStripSeparator1_Click(object sender, EventArgs e)
        {

        }
    }
}
