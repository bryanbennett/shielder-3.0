﻿namespace Shielder_3._0.Views
{
    partial class WorkView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.savePackageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newPackageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openPackageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.environmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addSpriteStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editSpritesStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.howToToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.packageTree = new System.Windows.Forms.TreeView();
            this.pnlLeft = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.pnlRight = new System.Windows.Forms.Panel();
            this.pnlDetails = new System.Windows.Forms.FlowLayoutPanel();
            this.lblNodeTitle = new System.Windows.Forms.Label();
            this.groupContext = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.stripNewItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stripNewDetail = new System.Windows.Forms.ToolStripMenuItem();
            this.stripAddString = new System.Windows.Forms.ToolStripMenuItem();
            this.stripAddBoolean = new System.Windows.Forms.ToolStripMenuItem();
            this.stripAddReference = new System.Windows.Forms.ToolStripMenuItem();
            this.stripAddImage = new System.Windows.Forms.ToolStripMenuItem();
            this.stripAddNumerical = new System.Windows.Forms.ToolStripMenuItem();
            this.stripAddXBool = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.stripDeleteGroup = new System.Windows.Forms.ToolStripMenuItem();
            this.itemContext = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.stripDeleteItem = new System.Windows.Forms.ToolStripMenuItem();
            this.packageContext = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.stripNewGroup = new System.Windows.Forms.ToolStripMenuItem();
            this.stripAddPack = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.stripAddSub = new System.Windows.Forms.ToolStripMenuItem();
            this.menu.SuspendLayout();
            this.pnlLeft.SuspendLayout();
            this.pnlRight.SuspendLayout();
            this.groupContext.SuspendLayout();
            this.itemContext.SuspendLayout();
            this.packageContext.SuspendLayout();
            this.SuspendLayout();
            // 
            // menu
            // 
            this.menu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.menu.Dock = System.Windows.Forms.DockStyle.None;
            this.menu.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.environmentToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
            this.menu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.menu.Size = new System.Drawing.Size(182, 24);
            this.menu.TabIndex = 6;
            this.menu.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.fileToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.savePackageToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.newPackageToolStripMenuItem,
            this.openPackageToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.fileToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.fileToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(38, 20);
            this.fileToolStripMenuItem.Text = "File";
            this.fileToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // savePackageToolStripMenuItem
            // 
            this.savePackageToolStripMenuItem.Name = "savePackageToolStripMenuItem";
            this.savePackageToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.savePackageToolStripMenuItem.Text = "Save Package";
            this.savePackageToolStripMenuItem.Click += new System.EventHandler(this.savePackageToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.saveAsToolStripMenuItem.Text = "Save As";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // newPackageToolStripMenuItem
            // 
            this.newPackageToolStripMenuItem.Name = "newPackageToolStripMenuItem";
            this.newPackageToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.newPackageToolStripMenuItem.Text = "New Package";
            this.newPackageToolStripMenuItem.Click += new System.EventHandler(this.newPackageToolStripMenuItem_Click);
            // 
            // openPackageToolStripMenuItem
            // 
            this.openPackageToolStripMenuItem.Name = "openPackageToolStripMenuItem";
            this.openPackageToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.openPackageToolStripMenuItem.Text = "Open Package";
            this.openPackageToolStripMenuItem.Click += new System.EventHandler(this.openPackageToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // environmentToolStripMenuItem
            // 
            this.environmentToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addSpriteStripMenuItem,
            this.editSpritesStripMenuItem,
            this.toolStripSeparator3,
            this.settingsToolStripMenuItem});
            this.environmentToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.environmentToolStripMenuItem.Name = "environmentToolStripMenuItem";
            this.environmentToolStripMenuItem.Size = new System.Drawing.Size(91, 20);
            this.environmentToolStripMenuItem.Text = "Environment";
            // 
            // addSpriteStripMenuItem
            // 
            this.addSpriteStripMenuItem.Name = "addSpriteStripMenuItem";
            this.addSpriteStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.addSpriteStripMenuItem.Text = "Add SpritePack";
            this.addSpriteStripMenuItem.Click += new System.EventHandler(this.addSpriteStripMenuItem_Click);
            // 
            // editSpritesStripMenuItem
            // 
            this.editSpritesStripMenuItem.Name = "editSpritesStripMenuItem";
            this.editSpritesStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.editSpritesStripMenuItem.Text = "Edit SpritePacks";
            this.editSpritesStripMenuItem.Click += new System.EventHandler(this.editSpritesStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(163, 6);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.settingsToolStripMenuItem.Text = "Settings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem,
            this.howToToolStripMenuItem});
            this.helpToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // howToToolStripMenuItem
            // 
            this.howToToolStripMenuItem.Name = "howToToolStripMenuItem";
            this.howToToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.howToToolStripMenuItem.Text = "How To";
            this.howToToolStripMenuItem.Click += new System.EventHandler(this.howToToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Control;
            this.label1.Location = new System.Drawing.Point(0, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Package Explorer";
            // 
            // packageTree
            // 
            this.packageTree.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.packageTree.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.packageTree.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.packageTree.ForeColor = System.Drawing.SystemColors.Control;
            this.packageTree.HideSelection = false;
            this.packageTree.LineColor = System.Drawing.Color.DimGray;
            this.packageTree.Location = new System.Drawing.Point(7, 49);
            this.packageTree.Name = "packageTree";
            this.packageTree.Size = new System.Drawing.Size(309, 618);
            this.packageTree.TabIndex = 9;
            this.packageTree.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.packageTree_AfterSelect);
            this.packageTree.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.packageTree_NodeMouseClick);
            // 
            // pnlLeft
            // 
            this.pnlLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.pnlLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlLeft.Controls.Add(this.label2);
            this.pnlLeft.Controls.Add(this.txtSearch);
            this.pnlLeft.Controls.Add(this.label1);
            this.pnlLeft.Controls.Add(this.packageTree);
            this.pnlLeft.Location = new System.Drawing.Point(3, 27);
            this.pnlLeft.Name = "pnlLeft";
            this.pnlLeft.Size = new System.Drawing.Size(326, 679);
            this.pnlLeft.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label2.Location = new System.Drawing.Point(17, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Search:";
            // 
            // txtSearch
            // 
            this.txtSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.txtSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSearch.ForeColor = System.Drawing.SystemColors.Control;
            this.txtSearch.Location = new System.Drawing.Point(79, 21);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(236, 21);
            this.txtSearch.TabIndex = 10;
            this.toolTip.SetToolTip(this.txtSearch, "Type here to search for a group or item.");
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            this.txtSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyDown);
            // 
            // pnlRight
            // 
            this.pnlRight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.pnlRight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlRight.Controls.Add(this.pnlDetails);
            this.pnlRight.Controls.Add(this.lblNodeTitle);
            this.pnlRight.Location = new System.Drawing.Point(335, 27);
            this.pnlRight.Name = "pnlRight";
            this.pnlRight.Size = new System.Drawing.Size(804, 679);
            this.pnlRight.TabIndex = 11;
            // 
            // pnlDetails
            // 
            this.pnlDetails.AllowDrop = true;
            this.pnlDetails.AutoScroll = true;
            this.pnlDetails.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.pnlDetails.Location = new System.Drawing.Point(3, 21);
            this.pnlDetails.Name = "pnlDetails";
            this.pnlDetails.Size = new System.Drawing.Size(796, 654);
            this.pnlDetails.TabIndex = 0;
            // 
            // lblNodeTitle
            // 
            this.lblNodeTitle.AutoSize = true;
            this.lblNodeTitle.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNodeTitle.ForeColor = System.Drawing.SystemColors.Control;
            this.lblNodeTitle.Location = new System.Drawing.Point(0, 2);
            this.lblNodeTitle.Name = "lblNodeTitle";
            this.lblNodeTitle.Size = new System.Drawing.Size(28, 13);
            this.lblNodeTitle.TabIndex = 8;
            this.lblNodeTitle.Text = "N/A";
            // 
            // groupContext
            // 
            this.groupContext.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupContext.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stripNewItem,
            this.stripNewDetail,
            this.stripAddSub,
            this.toolStripSeparator1,
            this.stripDeleteGroup});
            this.groupContext.Name = "groupContext";
            this.groupContext.Size = new System.Drawing.Size(204, 120);
            // 
            // stripNewItem
            // 
            this.stripNewItem.Name = "stripNewItem";
            this.stripNewItem.Size = new System.Drawing.Size(203, 22);
            this.stripNewItem.Text = "New Item";
            this.stripNewItem.Click += new System.EventHandler(this.stripNewItem_Click);
            // 
            // stripNewDetail
            // 
            this.stripNewDetail.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stripAddString,
            this.stripAddBoolean,
            this.stripAddReference,
            this.stripAddImage,
            this.stripAddNumerical,
            this.stripAddXBool});
            this.stripNewDetail.Name = "stripNewDetail";
            this.stripNewDetail.Size = new System.Drawing.Size(203, 22);
            this.stripNewDetail.Text = "New Detail";
            // 
            // stripAddString
            // 
            this.stripAddString.Name = "stripAddString";
            this.stripAddString.Size = new System.Drawing.Size(132, 22);
            this.stripAddString.Text = "String";
            this.stripAddString.Click += new System.EventHandler(this.stripAddString_Click);
            // 
            // stripAddBoolean
            // 
            this.stripAddBoolean.Name = "stripAddBoolean";
            this.stripAddBoolean.Size = new System.Drawing.Size(132, 22);
            this.stripAddBoolean.Text = "Boolean";
            this.stripAddBoolean.Click += new System.EventHandler(this.stripAddBoolean_Click);
            // 
            // stripAddReference
            // 
            this.stripAddReference.Name = "stripAddReference";
            this.stripAddReference.Size = new System.Drawing.Size(132, 22);
            this.stripAddReference.Text = "Reference";
            this.stripAddReference.Click += new System.EventHandler(this.stripAddReference_Click);
            // 
            // stripAddImage
            // 
            this.stripAddImage.Name = "stripAddImage";
            this.stripAddImage.Size = new System.Drawing.Size(132, 22);
            this.stripAddImage.Text = "Image";
            this.stripAddImage.Click += new System.EventHandler(this.stripAddImage_Click);
            // 
            // stripAddNumerical
            // 
            this.stripAddNumerical.Name = "stripAddNumerical";
            this.stripAddNumerical.Size = new System.Drawing.Size(132, 22);
            this.stripAddNumerical.Text = "Numerical";
            this.stripAddNumerical.Click += new System.EventHandler(this.stripAddNumerical_Click);
            // 
            // stripAddXBool
            // 
            this.stripAddXBool.Name = "stripAddXBool";
            this.stripAddXBool.Size = new System.Drawing.Size(132, 22);
            this.stripAddXBool.Text = "XBoolean";
            this.stripAddXBool.Click += new System.EventHandler(this.stripAddXBool_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(200, 6);
            this.toolStripSeparator1.Click += new System.EventHandler(this.toolStripSeparator1_Click);
            // 
            // stripDeleteGroup
            // 
            this.stripDeleteGroup.Name = "stripDeleteGroup";
            this.stripDeleteGroup.Size = new System.Drawing.Size(203, 22);
            this.stripDeleteGroup.Text = "Delete Selected Group";
            this.stripDeleteGroup.Click += new System.EventHandler(this.stripDeleteGroup_Click);
            // 
            // itemContext
            // 
            this.itemContext.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.itemContext.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stripDeleteItem});
            this.itemContext.Name = "itemContext";
            this.itemContext.Size = new System.Drawing.Size(143, 26);
            // 
            // stripDeleteItem
            // 
            this.stripDeleteItem.Name = "stripDeleteItem";
            this.stripDeleteItem.Size = new System.Drawing.Size(142, 22);
            this.stripDeleteItem.Text = "Delete Item";
            this.stripDeleteItem.Click += new System.EventHandler(this.stripDeleteItem_Click);
            // 
            // packageContext
            // 
            this.packageContext.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.packageContext.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stripNewGroup,
            this.stripAddPack,
            this.toolStripSeparator2});
            this.packageContext.Name = "packageContext";
            this.packageContext.Size = new System.Drawing.Size(162, 54);
            // 
            // stripNewGroup
            // 
            this.stripNewGroup.Name = "stripNewGroup";
            this.stripNewGroup.Size = new System.Drawing.Size(161, 22);
            this.stripNewGroup.Text = "New Group";
            this.stripNewGroup.Click += new System.EventHandler(this.stripNewGroup_Click);
            // 
            // stripAddPack
            // 
            this.stripAddPack.Name = "stripAddPack";
            this.stripAddPack.Size = new System.Drawing.Size(161, 22);
            this.stripAddPack.Text = "Add SpritePack";
            this.stripAddPack.Click += new System.EventHandler(this.stripAddPack_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(158, 6);
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.Filter = "Binary files|*.bin|All files|*.*";
            this.saveFileDialog.Title = "Choose a Destination";
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            this.openFileDialog.Filter = "Binary files|*.bin|All files|*.*";
            // 
            // stripAddSub
            // 
            this.stripAddSub.Name = "stripAddSub";
            this.stripAddSub.Size = new System.Drawing.Size(203, 22);
            this.stripAddSub.Text = "Add Subgroup";
            this.stripAddSub.Click += new System.EventHandler(this.stripAddSub_Click);
            // 
            // WorkView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlRight);
            this.Controls.Add(this.pnlLeft);
            this.Controls.Add(this.menu);
            this.Name = "WorkView";
            this.Load += new System.EventHandler(this.WorkView_Load);
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            this.pnlLeft.ResumeLayout(false);
            this.pnlLeft.PerformLayout();
            this.pnlRight.ResumeLayout(false);
            this.pnlRight.PerformLayout();
            this.groupContext.ResumeLayout(false);
            this.itemContext.ResumeLayout(false);
            this.packageContext.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.MenuStrip menu;
        protected System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        protected System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem howToToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem savePackageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem environmentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TreeView packageTree;
        private System.Windows.Forms.Panel pnlLeft;
        private System.Windows.Forms.Panel pnlRight;
        private System.Windows.Forms.Label lblNodeTitle;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.FlowLayoutPanel pnlDetails;
        private System.Windows.Forms.ContextMenuStrip groupContext;
        private System.Windows.Forms.ContextMenuStrip itemContext;
        private System.Windows.Forms.ContextMenuStrip packageContext;
        private System.Windows.Forms.ToolStripMenuItem stripDeleteItem;
        private System.Windows.Forms.ToolStripMenuItem stripNewItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem stripDeleteGroup;
        private System.Windows.Forms.ToolStripMenuItem stripNewGroup;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem stripNewDetail;
        private System.Windows.Forms.ToolStripMenuItem stripAddString;
        private System.Windows.Forms.ToolStripMenuItem stripAddBoolean;
        private System.Windows.Forms.ToolStripMenuItem stripAddReference;
        private System.Windows.Forms.ToolStripMenuItem stripAddImage;
        private System.Windows.Forms.ToolStripMenuItem stripAddNumerical;
        private System.Windows.Forms.ToolStripMenuItem stripAddXBool;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.ToolStripMenuItem openPackageToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ToolStripMenuItem stripAddPack;
        private System.Windows.Forms.ToolStripMenuItem editSpritesStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addSpriteStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem newPackageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stripAddSub;

    }
}
