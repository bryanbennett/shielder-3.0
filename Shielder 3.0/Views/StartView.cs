﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Shielder_3._0.Data;
using Shielder_3._0.Components;
using PaleHorseStudios.Shielder;

namespace Shielder_3._0.Views
{
    /// <summary>
    /// The initial, empty view upon loading Shielder.
    /// </summary>
    public partial class StartView : BaseView
    {
        private List<ShielderLink> recentLinks;
        private Dictionary<string, string> removes;
        private string removalMessage;


        public StartView(Main main)
            :base(main)
        {
            InitializeComponent();
            recentLinks = new List<ShielderLink>();
            removes = new Dictionary<string, string>();
            Init();
            timer.Start();
        }
        public StartView()
        {
            InitializeComponent();
        }
        private void StartView_Load(object sender, EventArgs e)
        {

        }
        private void Init()
        {
            BuildRecentList();
        }
        private void BuildRecentList()
        {
            pnlRecent.Controls.Clear();
            ShielderSettings ss = Main.Settings;
            Stack<string> recent = ss.Recent;
            Stack<string> invert = new Stack<string>();
            int count = recent.Count;

            for (int i = 0; i < count; i++)
            {
                string id = recent.Pop();
                string path = ss.GetPackagePath(id);
                string fileName = System.IO.Path.GetFileNameWithoutExtension(path);

                if (File.Exists(path))
                {
                    invert.Push(id);

                    if (i <= 10)
                    {
                        ShielderLink link = new ShielderLink(this, id);
                        link.Text = fileName;
                        Point location = new Point(0, (i * 27));
                        link.Location = location;
                        link.Visible = true;
                        link.BringToFront();
                        pnlRecent.Controls.Add(link);
                    }
                }
                else
                {
                    removes.Add(id, fileName);
                }
            }
            count = invert.Count;

            for (int i = 0; i < count; i++)
            {
                recent.Push(invert.Pop());
            }

            string packages = "";
            foreach (KeyValuePair<string, string> entry in removes)
            {
                packages += entry.Value + "\n";
                ss.RemovePackage(entry.Key);
            }
            removalMessage = "The following packages have changed location: \n\n" + packages + "\nThey will need to be opened manually to be accessed.";

        }
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Main.Close();
        }

        private void newPackage_Click(object sender, EventArgs e)
        {
            NewPackage();
        }
        private void loadPackageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenPackage();
        }
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox about = new AboutBox();
            about.Show();
        }

        private void howToToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
        public void SetPackage(Package p)
        {
            Main.NewPackage(p);
        }
        private void OpenPackage()
        {
            DialogResult result = openFileDialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                string fileName = openFileDialog.FileName;
                if (File.Exists(fileName))
                {
                    Stream streamer = File.OpenRead(fileName);
                    BinaryFormatter ds = new BinaryFormatter();
                    Package package = (Package)ds.Deserialize(streamer);
                    streamer.Close();
                    Main.NewPackage(package);
                }
            }
        }
        private void OpenPackage(string path)
        {
            if (File.Exists(path))
            {
                Stream streamer = File.OpenRead(path);
                BinaryFormatter ds = new BinaryFormatter();
                Package package = (Package)ds.Deserialize(streamer);
                streamer.Close();
                Main.NewPackage(package);
            }
        }
        private void NewPackage()
        {
            NewPackageEditor npe = new NewPackageEditor(this, System.Guid.NewGuid().ToString());
            npe.ShowDialog();
        }
        #region "Label Behavior"
        private void lblNewPackage_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            NewPackage();
        }

        private void lblOpenPackage_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            OpenPackage();
        }

        private void lblHelp_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //TODO
        }

        private void lblExit_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Main.Close();
        }
        /// <summary>
        /// This method is called from ShielderLinks that have been clicked to open the
        /// specified package declared by ID.
        /// </summary>
        /// <param name="id"></param>
        public void RecentLabelClick(string id)
        {
            ShielderSettings ss = Main.Settings;
            string path = ss.GetPackagePath(id);
            ss.ReorderRecent(id);
            if (File.Exists(path))
            {
                Stream streamer = File.OpenRead(path);
                BinaryFormatter ds = new BinaryFormatter();
                Package package = (Package)ds.Deserialize(streamer);
                streamer.Close();
                Main.NewPackage(package);
            }
            else
            {
                OKMessage ok = new OKMessage("Package Moved or Deleted", "The selected package cannot be found.  It may have been moved or deleted.  Try opening the package manually.");
                ok.ShowDialog();
                ss.RemovePackage(id);
                BuildRecentList();
            }
        }
        #endregion
        private void timer_Tick(object sender, EventArgs e)
        {
            timer.Stop();
            if (removes.Count > 0)
            {
                OKMessage ok = new OKMessage("Packages Moved or Deleted", removalMessage);
                ok.ShowDialog();
                removes.Clear();
            }
        }
    }
}
