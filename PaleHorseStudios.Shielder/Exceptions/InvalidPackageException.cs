﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaleHorseStudios.Shielder.Exceptions
{
    /// <summary>
    /// Thrown when the format or extension of the package is invalid.
    /// </summary>
    public class InvalidPackageException : Exception
    {

        public InvalidPackageException()
        {
        }

        public InvalidPackageException(string message)
            :base(message)
        {
        }
    }
}
