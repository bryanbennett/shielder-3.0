﻿
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaleHorseStudios.Shielder
{
    /// <summary>
    /// This class is the meat of the meta-data provided in games.  It is the end-node of the entire Package.
    /// </summary>
    [Serializable()]
    public class Item : ShielderNode
    {
        private Dictionary<string, ItemData> datum;

        public Item(string name, Group group)
            : base(name, Layer.Item)
        {
            group.Nodes.Add(this);
            ForeColor = Color.FromArgb(0, 0, 64, 128);
            datum = new Dictionary<string, ItemData>();
        }
        #region "Serialization Control"
        public Item(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            SerializationInfoEnumerator sie = info.GetEnumerator();
            while (sie.MoveNext())
            {
                SerializationEntry entry = sie.Current;
                string name = entry.Name;

                if (name.Equals("Datum"))
                {
                    datum = (Dictionary<string, ItemData>)entry.Value;
                }
            } 
        }
        protected override void Serialize(SerializationInfo si, StreamingContext context)
        {
            base.Serialize(si, context);
            si.AddValue("Datum", datum);
        }
        #endregion

        /// <summary>
        /// Returns the ItemData with the specified name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public ItemData GetData(string name)
        {
            return datum[name];
        }
        /// <summary>
        /// Checks to see if the item data exists yet or not.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool ItemDataExists(string name)
        {
            return datum.ContainsKey(name);
        }
        /// <summary>
        /// Saves the data in the collection of ItemData.
        /// </summary>
        /// <param name="data"></param>
        public void SaveData(ItemData data)
        {
            if (datum.ContainsKey(data.DefinitionName))
            {
                datum.Remove(data.DefinitionName);
                datum.Add(data.DefinitionName, data);
            }
            else
            {
                datum.Add(data.DefinitionName, data);
            }
        }
        public void RemoveData(string name)
        {
            datum.Remove(name);
        }

        #region "Properties"
        /// <summary>
        /// Returns the Group this item belongs to.
        /// </summary>
        public Group Group { get { return (Group)Parent; } }

        /// <summary>
        /// Returns the collection of ItemData associated with this item.
        /// </summary>
        public Dictionary<string, ItemData> Datum { get { return datum; } }
        #endregion
    }
}
