﻿
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace PaleHorseStudios.Shielder
{
    [Serializable()]
    public class Group : ShielderNode
    {
        private List<Definition> defs;
        private bool isSubGroup = false;

        /// <summary>
        /// Creates a Group and adds it to the Package.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="package"></param>
        public Group(string name, Package package)
            : base(name, Layer.Group)
        {
            package.Nodes.Add(this);
            defs = new List<Definition>();
        }
        /// <summary>
        /// Creates a Group and adds it to another Group.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="group"></param>
        public Group(string name, Group group)
            :base(name, Layer.Group)
        {
            group.Nodes.Add(this);
            defs = new List<Definition>();
            isSubGroup = true;
        }
        #region "Serialization Control"
        public Group(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            SerializationInfoEnumerator sie = info.GetEnumerator();
            while (sie.MoveNext())
            {
                SerializationEntry entry = sie.Current;
                string name = entry.Name;

                if (name.Equals("Defs"))
                {
                    defs = (List<Definition>)entry.Value;
                }
                if (name.Equals("SubGroup"))
                {
                    isSubGroup = (bool)entry.Value;
                }
            } 
        }
        protected override void Serialize(SerializationInfo si, StreamingContext context)
        {
            base.Serialize(si, context);
            si.AddValue("Defs", defs);
            si.AddValue("SubGroup", isSubGroup);
        }

        #endregion
        #region "Definitions"
        /// <summary>
        /// Adds a definition to this group's list of Definitions.
        /// </summary>
        /// <param name="def"></param>
        public void AddDefinition(Definition def)
        {
            bool exists = false;
            int count = defs.Count;
            for (int i = 0; i < count; i++)
            {
                Definition temp = defs[i];
                if (def.Name.Equals(temp.Name))
                {
                    defs[i] = def;
                    exists = true;
                }
            }
            if (!exists)
            {
                defs.Add(def);
            }
        }
        /// <summary>
        /// Replaces the first definition with the second definition.
        /// </summary>
        /// <param name="def"></param>
        public void AddDefinition(Definition oldDef, Definition def)
        {
            bool exists = false;
            int count = defs.Count;
            for (int i = 0; i < count; i++)
            {
                Definition temp = defs[i];
                if (oldDef.Name.Equals(temp.Name))
                {
                    defs[i] = def;
                    exists = true;
                }
            }
            if (!exists)
            {
                defs.Add(def);
            }
        }

        /// <summary>
        /// Removes a definition from this group's list of Definitions.
        /// </summary>
        /// <param name="def"></param>
        public void RemoveDefinition(Definition def)
        {
            defs.Remove(def);
        }
        /// <summary>
        /// Removes the definition from this group and removes the definitions from all the items passed in.
        /// </summary>
        /// <param name="def"></param>
        /// <param name="items"></param>
        public void RemoveDefinition(Definition def, List<Item> items)
        {
            defs.Remove(def);

            foreach (Item item in items)
            {
                Dictionary<string, ItemData> datum = item.Datum;
                if (datum.ContainsKey(def.Name))
                {
                    datum.Remove(def.Name);
                }
            }
        }
        /// <summary>
        /// Returns the list of definitions contained by this group and, if applicable, its parent's definitions.
        /// </summary>
        /// <returns></returns>
        public List<Definition> GetDefinitions()
        {

            if (isSubGroup)
            {
                Group parent = (Group)Parent;
                List<Definition> parentdefs = parent.GetDefinitions();
                List<Definition> conglomerate = new List<Definition>();

                foreach (Definition def in parentdefs)
                {
                    conglomerate.Add(def);
                }
                foreach (Definition def in defs)
                {
                    conglomerate.Add(def);
                }
                return conglomerate;
            }
            else
            {
                return defs;
            }
        }
        #endregion

        /// <summary>
        /// Returns all the Item nodes that belong to this Group (including grand-children Item nodes).
        /// </summary>
        /// <returns></returns>
        public List<Item> GetItems()
        {
            List<Item> items = new List<Item>();

            foreach (ShielderNode node in Nodes)
            {
                if (node.Layer == Layer.Item)
                {
                    Item temp = (Item)node;
                    items.Add(temp);
                }
                else if (node.Layer == Layer.Group)
                {
                    Group temp = (Group) node;
                    List<Item> subItems = temp.GetItems();
                    foreach (Item entry in subItems)
                    {
                        items.Add(entry);
                    }
                }
            }
            return items;
        }
        /// <summary>
        /// Returns whether or not the definition name is already in use.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool IsUniqueDefinitionName(string name)
        {
            string lower = name.ToLower();
            List<Definition> conglomerate = GetDefinitions();

            foreach (Definition def in conglomerate)
            {
                string detail = def.Name.ToLower();

                if (lower.Equals(detail))
                {
                    return false;
                }
            }
            return true;
        }
        /// <summary>
        /// Modifies the default value of the definition based on the specified ItemData.
        /// </summary>
        /// <param name="def"></param>
        public void SaveDefault(ItemData data)
        {
            int count = defs.Count;
            for (int i = 0; i < count; i++)
            {
                string defName = data.DefinitionName;
                string mainDef = defs[i].Name;

                if (defName.Equals(mainDef))
                {
                    Definition def = defs[i];
                    def.Default = data.Value;
                }
            }
        }

        /// <summary>
        /// Updates the order of Definitions to 'save' the order in which the details constructed
        /// from the definitions to be added to the panel in the correct order.
        /// </summary>
        /// <param name="controls"></param>
        public void UpdateControlOrder(System.Windows.Forms.Form.ControlCollection controls)
        {
            List<Definition> temp = new List<Definition>();
            int count = controls.Count;
            int defCount = defs.Count;

            for (int i = 0; i < count; i++)
            {
                string name = controls[i].Name;
                for (int j = 0; j < defCount; j++)
                {
                    if (name.Equals(defs[j].Name))
                    {
                        temp.Add(defs[j]);
                        defs.RemoveAt(j);
                        break;
                    }
                }
            }
            defs = temp;
        }

        #region "Properties"
        /// <summary>
        /// Returns the Package node this group belongs to.
        /// </summary>
        public Package Package { get { return (Package)Parent; } }

        /// <summary>
        /// Returns the detail definitions contained in this group.
        /// </summary>
        public List<Definition> Definitions { get { return defs; } }

        /// <summary>
        /// Returns whether or not this group is a subgroup of another group.
        /// If it is a subgroup, then its parent node is a group.
        /// </summary>
        public bool SubGroup { get { return isSubGroup; } }
        #endregion
    }
}
