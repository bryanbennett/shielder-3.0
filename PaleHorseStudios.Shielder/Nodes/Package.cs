﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PaleHorseStudios.Shielder
{
    [Serializable()]
    public class Package : ShielderNode
    {
        private string path;
        private bool hasPath;
        private string guid;
        private SettingsData settings;
        private Dictionary<string, SpritePack> spritePacks;

        public Package(string name, string guid)
            :base(name, Layer.Package)
        {
            settings = new SettingsData(this);
            this.guid = guid;
            spritePacks = new Dictionary<string, SpritePack>();
        }
        #region "Serialization Control"
        public Package(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            SerializationInfoEnumerator sie = info.GetEnumerator();
            while (sie.MoveNext())
            {
                SerializationEntry entry = sie.Current;
                string name = entry.Name;

                if (name.Equals("FilePath"))
                {
                    FilePath = (string)entry.Value;
                }
                if (name.Equals("Settings"))
                {
                    settings = (SettingsData)entry.Value;
                }
                if (name.Equals("ID"))
                {
                    guid = (string)entry.Value;
                }
                if (name.Equals("Sprites"))
                {
                    spritePacks = (Dictionary<string, SpritePack>)entry.Value;
                }
            } 
        }
        protected override void Serialize(SerializationInfo si, StreamingContext context)
        {
            base.Serialize(si, context);
            si.AddValue("FilePath", FilePath);
            si.AddValue("Settings", settings);
            si.AddValue("ID", guid);
            si.AddValue("Sprites", spritePacks);
        }
        #endregion
        #region "Environment Settings"
        /// <summary>
        /// Updates all the nodes with the most current settings.
        /// </summary>
        public void UpdateSettings()
        {
            UpdateSettings(this);
        }
        private void UpdateSettings(ShielderNode node)
        {
            if (node.Layer == Layer.Package)
            {
                node.ForeColor = settings.PackageColor;
            }
            else if (node.Layer == Layer.Group)
            {
                node.ForeColor = settings.GroupColor;
            }
            else if (node.Layer == Layer.Item)
            {
                node.ForeColor = settings.ItemColor;
            }

            if (node.Nodes.Count > 0)
            {
                foreach (ShielderNode n in node.Nodes)
                {
                    UpdateSettings(n);
                }
            }
        }
        #endregion

        /// <summary>
        /// Adds a sprite pack to the package.
        /// </summary>
        /// <param name="sp"></param>
        public void AddSpritePack(SpritePack sp)
        {
            spritePacks.Add(sp.Name, sp);
        }
        /// <summary>
        /// Returns whether or not the name of the specified pack already exists.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool ContainsSpritePack(string name)
        {
            return spritePacks.ContainsKey(name);
        }
        #region "Properties"
        /// <summary>
        /// Gets or sets the path to the binary package.
        /// </summary>
        public string FilePath { get { return path; } set { path = value; hasPath = true; } }

        /// <summary>
        /// Returns whether or not this package has a desination path upon saving.
        /// </summary>
        public bool HasPath { get { return hasPath; } }

        /// <summary>
        /// Gets the SettingsData associated with this package.
        /// </summary>
        public SettingsData Settings { get { return settings; } }

        /// <summary>
        /// Gets the numerical ID provided by the Shielder application.
        /// </summary>
        public string ID { get { return guid; } }

        /// <summary>
        /// Returns the collection of SpritePacks contained in this package.
        /// </summary>
        public Dictionary<string, SpritePack> SpritePacks { get { return spritePacks; } }
        #endregion
    }
}
