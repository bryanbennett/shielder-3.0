﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaleHorseStudios.Shielder
{
    [Serializable()]
    public abstract class ShielderNode : TreeNode
    {
        private Layer level;

        public ShielderNode(string name, Layer level)
        {
            Text = name;
            this.level = level;
        }
        #region "Serialization Control"
        public ShielderNode(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            SerializationInfoEnumerator sie = info.GetEnumerator();

            while (sie.MoveNext())
            {
                SerializationEntry entry = sie.Current;
                string name = entry.Name;

                if (name.Equals("Layer"))
                {
                    level = (Layer)entry.Value;
                }
            }     
        }
        protected override void Serialize(SerializationInfo si, StreamingContext context)
        {
            base.Serialize(si, context);

            si.AddValue("Layer", level);
        }
        #endregion

        #region "Properties"
        /// <summary>
        /// Returns the layer in which this node resides.  Basically, it returns the
        /// type of node.
        /// </summary>
        public Layer Layer { get { return level; } }
        #endregion
    }
}
