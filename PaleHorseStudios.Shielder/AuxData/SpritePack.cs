﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaleHorseStudios.Shielder
{
    [Serializable()]
    public class SpritePack
    {
        private string name;
        private Dictionary<string, byte[]> images;

        public SpritePack(string name, string path)
        {
            images = new Dictionary<string, byte[]>();
            this.name = name;

            string[] files = System.IO.Directory.GetFiles(path);

            foreach (string s in files)
            {
                string fileName = System.IO.Path.GetFileName(s);
                Image img = Image.FromFile(s);
                images.Add(fileName, ImageToByte(img));
            }
        }
        /// <summary>
        /// Converts an Image to a byte array formatted according to the image's format.
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        public static byte[] ImageToByte(Image img)
        {
            MemoryStream ms = new MemoryStream();
            img.Save(ms, img.RawFormat);
            return ms.ToArray();
        }
        /// <summary>
        /// Converts a byte array into an Image.
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public static Image ByteToImage(byte[] array)
        {
            MemoryStream ms = new MemoryStream(array);
            Image img = Image.FromStream(ms);
            return img;
        }
        /// <summary>
        /// Returns the image from the dictionary.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public Image GetImage(string name)
        {
            return ByteToImage(images[name]);
        }
        /// <summary>
        /// Returns the list of images in this item pack.  The "Tag" property of each image contains the original
        /// name of the file.
        /// </summary>
        /// <returns></returns>
        public List<Image> ToList()
        {
            List<Image> list = new List<Image>();

            foreach (KeyValuePair<string, byte[]> entry in images)
            {
                Image img = ByteToImage(entry.Value);
                img.Tag = entry.Key;
                list.Add(img);
            }
            return list;
        }
        /// <summary>
        /// Returns a dictionary of images in this pack.  The string portion of the pair is the name
        /// of the image.
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, Image> ToDictionary()
        {
            Dictionary<string, Image> list = new Dictionary<string, Image>();
            foreach (KeyValuePair<string, byte[]> entry in images)
            {
                Image img = ByteToImage(entry.Value);
                list.Add(entry.Key, img);
            }
            return list;
        }
        /// <summary>
        /// Adds the byte array to the list of byte arrays with the specified key.  Returns true if the addition succeeded.  False otherwise.
        /// </summary>
        /// <param name="imgArray"></param>
        public bool AddByteArray(string key, byte[] imgArray)
        {
            if (!images.ContainsKey(key))
            {
                images.Add(key, imgArray);
                return true;
            }
            else
            {
                Console.WriteLine("Byte array with key [" + key + "] already exists!");
                return false;
            }
        }
        /// <summary>
        /// Removes the image with the specified key.
        /// </summary>
        /// <param name="key"></param>
        public void RemoveImage(string key)
        {
            images.Remove(key);
        }
        /// <summary>
        /// Returns whether or not the specified image is contained within this spritepack.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool ContainsImage(string key)
        {
            return images.ContainsKey(key);
        }
        #region "Properties"

        /// <summary>
        /// Returns the name of the sprite pack.
        /// </summary>
        public string Name { get { return name; } }
        #endregion
    }
}
