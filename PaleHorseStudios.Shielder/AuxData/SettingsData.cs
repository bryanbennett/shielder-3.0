﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaleHorseStudios.Shielder
{
    [Serializable()]
    public class SettingsData
    {
        private Color packageColor = System.Drawing.Color.White;
        private Color groupColor = System.Drawing.Color.White;
        private Color itemColor = System.Drawing.Color.FromArgb(0, 0, 64, 128);
        private Color changedColor = System.Drawing.Color.Firebrick;
        private Package package;

        public SettingsData(Package package)
        {
            this.package = package;
        }
        public void Update()
        {
            package.UpdateSettings();
        }
        #region "Properties"
        /// <summary>
        /// Gets or sets the color of packages.
        /// </summary>
        public Color PackageColor { get { return packageColor; } set { packageColor = value; } }
        /// <summary>
        /// Gets or sets the color of groups.
        /// </summary>
        public Color GroupColor { get { return groupColor; } set { groupColor = value; } }
        /// <summary>
        /// Gets or sets the color of items.
        /// </summary>
        public Color ItemColor { get { return itemColor; } set { itemColor = value; } }

        /// <summary>
        /// Gets or sets the color that is displayed if a node needs to be highlighted.
        /// </summary>
        public Color ChangedColor { get { return changedColor; } set { changedColor = value; } }
        #endregion
    }
}
