﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaleHorseStudios.Shielder
{
    public enum Datatype
    {
        Numeric,
        Boolean,
        XBoolean,
        String,
        Reference,
        Image,
    }
}
