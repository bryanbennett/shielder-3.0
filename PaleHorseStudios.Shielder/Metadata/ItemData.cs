﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaleHorseStudios.Shielder
{
    [Serializable()]
    public class ItemData
    {
        private string value = "";
        private string name = "";
        private string group = "";
        public ItemData(string name)
        {
            this.name = name;
        }
        #region "Properties"
        /// <summary>
        /// Gets or sets the value of the ItemData.
        /// </summary>
        public string Value { get { return value; } set { this.value = value; } }

        /// <summary>
        /// Gets or sets the name of the Definition this ItemData belongs to.
        /// </summary>
        public string DefinitionName { get { return name; } set { name = value; } }

        /// <summary>
        /// Gets or sets the name of the Group that the Definition that defines this ItemData
        /// belongs to.
        /// </summary>
        public string GroupName { get { return group; } set { group = value; } }
        #endregion
    }
}
