﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaleHorseStudios.Shielder.Metadata
{
    public class MetaGroup
    {
        private string name;
        private List<MetaItem> items;

        public MetaGroup(Group group)
        {
            this.name = group.Text;
            GenerateTree(group);
        }
        private void GenerateTree(Group group)
        {
            items = new List<MetaItem>();

            foreach (ShielderNode node in group.Nodes)
            {
                NodeSearch(node);
            }
        }
        private void NodeSearch(ShielderNode node)
        {
            if (node.Layer == Layer.Item)
            {
                MetaItem item = new MetaItem((Item)node);
                items.Add(item);
            }
            else if (node.Layer == Layer.Group)
            {
                foreach (ShielderNode sNode in node.Nodes)
                {
                    NodeSearch(sNode);
                }
            }
        }
        /// <summary>
        /// Gets a list of items under this group that does not include items found in subgroups.
        /// </summary>
        /// <returns></returns>
        public List<MetaItem> ExclusiveItems()
        {
            List<MetaItem> exItems = new List<MetaItem>();

            foreach (MetaItem item in items)
            {
                if (item.ParentGroup.Equals(Name))
                {
                    exItems.Add(item);
                }
            }
            return exItems;
        }

        #region "Properties"
        /// <summary>
        /// Returns a list containing all the items below this group, including
        /// items found in this group's subgroups.
        /// </summary>
        public List<MetaItem> Items { get { return items; } }

        /// <summary>
        /// Returns the name of this group.
        /// </summary>
        public string Name { get { return name; } }
        #endregion
    }
}
