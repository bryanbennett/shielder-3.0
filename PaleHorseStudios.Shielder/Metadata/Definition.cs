﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaleHorseStudios.Shielder
{
    [Serializable()]
    /// <summary>
    /// This class is used to reconstruct the details and their default values for groups and items.
    /// The properties contained in this class may or may not be relevant to the Detail it 
    /// is trying to reconstruct.
    /// </summary>
    public class Definition
    {
        private string name = "Null";
        private Datatype type;
        private string defaultValue = "0";
        private decimal maxValue = 100;
        private decimal minValue = 0;
        private int decimalPlaces = 0;
        private int charLimit = 20;
        private bool nonEmpty = false;
        private string[] list;
        private string groupRef = "";
        private string parentDef = "";
        private bool isChild = false;
        private string spritePack = "";

        public Definition(string name, Datatype type)
        {
            this.name = name;
            this.type = type;    
        }
        #region "Properties"
        /// <summary>
        /// Gets or sets the name associated with this definition.
        /// </summary>
        public string Name { get { return name; } set { name = value; } }

        /// <summary>
        /// Returns the type associated with this definition.
        /// </summary>
        public Datatype Type { get { return type; } }

        /// <summary>
        /// Gets or sets the default value of this definition.
        /// </summary>
        public string Default { get { return defaultValue; } set { defaultValue = value; } }

        /// <summary>
        /// Gets or sets the maximum value of a relevant detail for this definition.
        /// </summary>
        public decimal MaxValue { get { return maxValue; } set { maxValue = value; } }

        /// <summary>
        /// Gets or sets the minimum value of a relevant detail (such as the NumericDetail) 
        /// for this definition.
        /// </summary>
        public decimal MinValue { get { return minValue; } set { minValue = value; } }

        /// <summary>
        /// Gets or sets the character limit of a relevant detail (such as the StringDetail) 
        /// for this definition.
        /// </summary>
        public int CharLimit { get { return charLimit; } set { charLimit = value; } }

        /// <summary>
        /// Gets or sets whether or not the detail can be empty.
        /// </summary>
        public bool NonEmpty { get { return nonEmpty; } set { nonEmpty = value; } }

        /// <summary>
        /// Gets or sets the list for a relevant detail (such as the XBooleanDetail) to be
        /// displayed in a combo box.
        /// </summary>
        public string[] List { get { return list; } set { list = value; } }

        /// <summary>
        /// Gets or sets the reference of another group for a relevant detail (such as the ReferenceDetail)
        /// to be displayed in a combo box.
        /// </summary>
        public string Reference { get { return groupRef; } set { groupRef = value; } }

        /// <summary>
        /// Gets or sets the parent definition if this definition should be created under another detail.
        /// </summary>
        public string ParentDefinition { get { return parentDef; } set { parentDef = value; isChild = true; } }

        /// <summary>
        /// Returns whether or not this definition is a child definition.
        /// </summary>
        public bool IsChild { get { return isChild; } }

        /// <summary>
        /// Gets or sets the amount of decimal places this detail has.
        /// </summary>
        public int DecimalPlaces { get { return decimalPlaces; } set { decimalPlaces = value; } }

        /// <summary>
        /// Gets or sets the name of the sprite pack referenced.
        /// </summary>
        public string SpritePack { get { return spritePack; } set { spritePack = value; } }
        #endregion
    }
}
