﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaleHorseStudios.Shielder.Metadata
{
    public class MetaItem
    {
        private Dictionary<string, ItemData> datum;
        private string name;
        private string groupName;

        public MetaItem(Item item)
        {
            this.name = item.Text;
            this.datum = item.Datum;
            this.groupName = item.Parent.Text;
        }

        #region "Properties"
        /// <summary>
        /// Gets the name of this MetaItem.
        /// </summary>
        public string Name { get { return name;} }

        /// <summary>
        /// Gets the collection of data that this MetaItem contains.
        /// </summary>
        public Dictionary<string, ItemData> Datum { get { return datum; } }

        /// <summary>
        /// Returns a list of the name of Details.
        /// </summary>
        public List<string> Labels { get { return datum.Keys.ToList<string>(); } }

        /// <summary>
        /// Returns the name of the group that directly contains this item.
        /// </summary>
        public string ParentGroup { get { return groupName; } }

        #endregion
    }
}
