﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using PaleHorseStudios.Shielder.Metadata;

namespace PaleHorseStudios.Shielder
{
    public class ShielderPackage
    {
        private string path;
        private Package package;
        private Dictionary<string, MetaGroup> groups;

        public ShielderPackage(string path)
        {
            this.path = path;
            if (File.Exists(path))
            {
                Stream stream = File.OpenRead(path);
                BinaryFormatter bf = new BinaryFormatter();
                package = (Package)bf.Deserialize(stream);
                stream.Close();
            }
            else
            {
                throw new FileNotFoundException("The path to the Shielder package was invalid.");
            }
            InitializeCollections();
            ProcessPackage();
        }
        private void InitializeCollections()
        {
            groups = new Dictionary<string, MetaGroup>();
        }
        private void ProcessPackage()
        {
            //Since groups generate items and subgroups, this will generate the entire package.
            foreach (ShielderNode node in package.Nodes)
            {
                if (node.Layer == Layer.Group)
                {
                    MetaGroup group = new MetaGroup((Group)node);
                    groups.Add(group.Name,group);
                    GroupCollect(node);
                }
            }
        }
        private void GroupCollect(ShielderNode node)
        {
            if (node.Layer == Layer.Group)
            {
                MetaGroup group = new MetaGroup((Group)node);
                if (!groups.ContainsKey(group.Name))
                {
                    groups.Add(group.Name, group);
                }

                foreach (ShielderNode sNode in node.Nodes)
                {
                    GroupCollect(sNode);
                }
            }
        }
        /// <summary>
        /// Returns a list of items that are contained in a group.  If the boolean argument is true,
        /// it will return items found ONLY in the group specified and not items found in its subgroups.  If it is false,
        /// it returns all items beneath it, including items found in subgroups.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public List<MetaItem> GetItemsOfGroup(string name, bool exclusive)
        {
            if (exclusive)
            {
                return groups[name].ExclusiveItems();
            }
            else
            {
                return groups[name].Items;
            }
        }
        #region "Properties"

        /// <summary>
        /// Gets the path of the package used to create this ShielderPackage.
        /// </summary>
        public string Path { get { return path; } }

        /// <summary>
        /// Returns a list of the group names contained in this Shielder Package.
        /// </summary>
        public List<string> GroupNames { get { return groups.Keys.ToList<string>(); } }

        /// <summary>
        /// Returns a collection of groups in this Shielder Package.
        /// </summary>
        public Dictionary<string, MetaGroup> Groups { get { return groups; } }

        #endregion

    }
}
