﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PaleHorseStudios.Shielder;
using PaleHorseStudios.Shielder.Metadata;

namespace TestProject
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            ShielderPackage package = new ShielderPackage("C:\\Users\\Bryan\\Desktop\\Test2.bin");

            List<string> groupNames = package.GroupNames;
            string message = "";
            foreach (string name in groupNames)
            {
                message += name + "\n";
            }
            message += "\n";
            List<MetaItem> items = package.GetItemsOfGroup("Items", true);
            foreach (MetaItem item in items)
            {
                message += item.Name + "\n";
            }
            rtbTest.Text = message + "\n";
        }
    }
}
